/**
 * Created by zhoulanhong on 1/1/17.
 */
var schedule = require('node-schedule');
var PttEnterprise = require('../models/ptt-enterprise');
var co = require('co');
var Event = require('../signal/event');

module.exports = function (loginUser) {
    //run this job every one minute
    schedule.scheduleJob('1 * * * * *', function () {

        co(function * () {
                var enterprises = yield PttEnterprise.find({"mode.modetype": "1"}).exec();
                var etTime = Date.now();
                //指挥时间已过期
                if (enterprises && enterprises.length > 0) {
                    for (let i in enterprises) {
                        if (etTime > enterprises[i].mode.endTime) {
                            console.log("----------------- enterprise : " + enterprises[i].idNumber);
                            yield PttEnterprise.findOneAndUpdate({"idNumber": enterprises[i].idNumber}, {"mode": null}, {new: true}).exec();
                            let userCommanderSocket = loginUser.commanders.get(enterprises[i].idNumber);
                            console.log("--- found Commander : " + userCommanderSocket)
                            if (userCommanderSocket) {
                                console.log("on line status emited");
                                userCommanderSocket.emit(Event.Send.COMMAND_MODE_END, {msg: "command end"});
                            }
                            process.signalServer.onEnterpriseModeChanged(enterprises[i]._id);
                        }
                    }
                }

            }
        ).catch(function (err) {
            console.log(err);
            res.json(err);
        });

    });
}