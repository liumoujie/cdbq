/**
 * Created by suke on 15/12/9.
 */
var crypto = require('crypto');
var Promise = require('bluebird');
var request = require('request');

var PttUtil = {

    //MD5生成器
    md5: function (text) {
        return crypto.createHash('md5').update(text).digest('hex');
    },

    //将mongodb的ISODate转化为yyyy-mm-dd HH:MM:SS 的格式
    formatISODate: function (str) {
        var ndate = new Date();
        ndate.setTime(Date.parse(str));
        return ndate.getUTCFullYear() + "-" +
            (ndate.getUTCMonth() > 8 ? (ndate.getUTCMonth() + 1) : ("0" + (ndate.getUTCMonth() + 1))) + "-" +
            (ndate.getUTCDate() > 9 ? (ndate.getUTCDate() ) : ("0" + (ndate.getUTCDate() ))) + " " +
            (ndate.getHours() > 9 ? (ndate.getHours() ) : ("0" + (ndate.getHours() ))) + ":" +
            (ndate.getMinutes() > 9 ? (ndate.getMinutes() ) : ("0" + (ndate.getMinutes() ))) + ":"
            + (ndate.getSeconds() > 9 ? (ndate.getSeconds() ) : ("0" + (ndate.getSeconds() )));
    },

    //将YYYY-MM-DD型的日期，转换为以 UTC timestamp
    transferYYYY_MM_DDToTimestamp: function (YYYYMMDD) {
        var curTimeStamp = Date.parse(YYYYMMDD);
        return curTimeStamp;
    },

    //将UTC timestamp 转换为以YYYY-MM-DD的日期格式
    getYYYY_MM_DD: function (tm) {
        var now = new Date();
        now.setTime(tm);
        var m = (now.getMonth() + 1) < 10 ? "0" + (now.getMonth() + 1) : (now.getMonth() + 1);
        var d = now.getDate() < 10 ? "0" + now.getDate() : now.getDate();
        return '' + now.getFullYear() + '-' + m + '-' + d;
    },

    //将UTC timestamp 转换为以YYYYMMDD的日期格式
    getYYYYMMDD: function (tm) {
        var now = new Date();
        if (tm) {
            now.setTime(tm);
        }
        var m = (now.getMonth() + 1) < 10 ? "0" + (now.getMonth() + 1) : (now.getMonth() + 1);
        var d = now.getDate() < 10 ? "0" + now.getDate() : now.getDate();
        return '' + now.getFullYear() + '' + m + '' + d;
    },

    //将YYYY/MM/DD型的日期，转换为以 GMT+8 时区对应的 timestamp
    transferToTimestamp: function (YYYYMMDD) {
        var timeZone = (new Date()).getTimezoneOffset();
        var curTimeStamp = Date.parse(YYYYMMDD);
        if (timeZone === -480) {//GMT+8
            return curTimeStamp;
        } else {
            return curTimeStamp + timeZone + 480; // -(-480)
        }
    },

    //检查电话号码
    checkPhone: function (phoNumber) {
        var reg = /^1\d{10}$/;
        return reg.test(phoNumber);
    },

    //检查电子邮箱
    checkMail: function (mail) {
        var reg = /^(\w-*\.*)+@(\w-?)+(\.\w{1,})+$/;
        return reg.test(mail);
    },

    //通过百度地图api将地理位置转换成地址
    queryLoc: function (lng, lat) {
        return new Promise(function (resolve, reject) {
            let url = "http://api.map.baidu.com/geocoder/v2/?ak=7jQeaeyVKbyYAVVVqNSfC6wxLkE9vYhw&location="
                + lat + "," + lng + "&output=json&pois=0";
            request(url, function (error, response, body) {
                if (!error && response.statusCode == 200) {
                    try {
                        //转换成 json 再送传出去
                        let json = JSON.parse(body);
                        resolve(json);
                    } catch (e) {
                        resolve({
                            "status": 0,
                            "result": {
                                "formatted_address": "没有找到对应的地址"
                            }
                        });
                    }
                } else {
                    resolve({
                        "status": 0,
                        "result": {
                            "formatted_address": "没有找到对应的地址"
                        }
                    });
                }
            });
        });
    },

    isNullOrSpace: function (s) {
        if (s == null || s == undefined || s.trim().length == 0 || s == "undefined" || s.toLowerCase() == "null") {
            return true;
        }
        return false;
    }


};

module.exports = PttUtil;
