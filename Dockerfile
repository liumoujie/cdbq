FROM node:6.11.1
MAINTAINER Fanchao Liu

WORKDIR /app

# Install base system packages
RUN apt-get -y update && \
    apt-get -y install git python build-essential && \
    apt-get -y install vim && \
    npm install bower -g

# Copy configuration files
COPY bower.json package.json npm-shrinkwrap.json ./

# Install required packages
RUN npm install && \
    bower --allow-root install
    
ARG BUILD_NUMBER=1.0-snapshot
RUN echo $BUILD_NUMBER > .version

# Copy source code
COPY . ./

# Environment settings
ENV MONGODB_PATH mongodb://db
ENV VOICE_SERVER_URL netptt.cn
ENV VOICE_SERVER_PORT 5000
ENV VOICE_SERVER_TCP_PORT 6000
ENV KURENTO_SERVER_URL netptt.cn
ENV KURENTO_SERVER_PORT 8433
ENV PUSH_SERVER_ADDR localhost:4000
ENV PROD_APP_URL http://netptt.cn:10005/app/latest/

EXPOSE 3000

ENTRYPOINT /app/bin/www
