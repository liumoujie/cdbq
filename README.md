# 工程介绍 #

工程为基于NodeJS实现的对讲“信号服务器” 【区别于音频处理和分发的“媒体服务器”】，主要负责对讲的组管理，用户管理，对讲状态管理，同时工程也包括了后台管理系统。

### 技术方案 ###

* WEB基于Express开发。
* 和手机客户端通信使用Socket.IO作为底层库。
* 数据库系统使用MongoDB。
* 包管理使用 NPM , Bower.

### 如何运行 ###

* git clone git@bitbucket.org:xianzhiteam/ptt_server_signal.git
* npm install
* bower install
* npm start

### 构建发布版本 ###

首先要确保Docker能正常运作

```
$ docker build -t dev.simophin.net/ptt_server_signal . 
# docker push dev.simophin.net/ptt_server_signal
```

源服务器的用户名密码参照最新的服务器配置文档

