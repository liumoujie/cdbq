/**
 * Created by zhoulanhong on 11/6/18.
 */
let conf = require('../config/cdbq_config');
let request = require('sync-request');

var logger = require('../config/log4js').getLogger('QueryBillDetail');

module.exports = function (kdCookie, billNumber, billType) {
    let supprotBillType = ['STK_InStock'];

    if (supprotBillType.indexOf(billType) < 0) {
        return {
            message: "暂不支持付款单以外的审批类型",
            error: {
                status: "错误",
                status: "正在开发中"
            }
        };
    }

    let queryBillDetail = request('POST', conf.baseUrl + conf.webApi.view, {
        headers: {
            'Cookie': kdCookie
        },
        json: {
            "parameters": [
                billType,
                "{\"CreateOrgId\":\"0\",\"Number\":\"" + billNumber + "\",\"Id\":\"\"}"
            ]
        }
    }).getBody('utf8');

    logger.info(JSON.stringify(conf.baseUrl + conf.webApi.view));
    logger.info(JSON.stringify(kdCookie));
    logger.info(JSON.stringify({
        "parameters": [
            billType,
            "{\"CreateOrgId\":\"0\",\"Number\":\"" + billNumber + "\",\"Id\":\"\"}"
        ]
    }));
    //logger.info(JSON.stringify(queryBillDetail));
    return queryBillDetail;
}