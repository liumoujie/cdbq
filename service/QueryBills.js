/**
 * Created by zhoulanhong on 11/6/18.
 */
let conf = require('../config/cdbq_config');
let request = require('sync-request');

var logger = require('../config/log4js').getLogger('QueryBills');

module.exports=function(kdCookie, billDate, billType){
    let supprotBillType = ['STK_InStock'];

    if (supprotBillType.indexOf(billType) < 0) {
        return {
            message: "暂不支持付款单以外的审批类型",
            error: {
                status: "错误",
                status: "正在开发中"
            }
        };
    }

    let queryBills = request('POST', conf.baseUrl + conf.webApi.query, {
            headers: {
                'Cookie': kdCookie
            },
            json: {
                "parameters": [
                    "STK_InStock",

                    "{\"FormId\":\"STK_InStock\",\"FieldKeys\":\"FBillNo\",\"FilterString\":\"FDate='2018-11-06'\",\"OrderString\":\"\",\"TopRowCount\":\"0\",\"StartRow\":\"0\",\"Limit\":\"0\"}"

                    //"{\"FormId\":\"STK_InStock\",\"FieldKeys\":\"FBillNo\",\"FilterString\":\"FDate ='"+billDate+"'\",\"OrderString\":\"\",\"TopRowCount\":\"0\",\"StartRow\":\"0\",\"Limit\":\"0\"}"
                ]
            }
        }).getBody('utf8');
    logger.info(JSON.stringify(conf.baseUrl + conf.webApi.query));
    logger.info(JSON.stringify(kdCookie));
    //logger.info(JSON.stringify( {
    //    "parameters": [
    //        billType,
    //        "{\"FormId\":\"STK_InStock\",\"FieldKeys\":\"FBillNo\",\"FilterString\":\"FDate ='"+billDate+"'\",\"OrderString\":\"\",\"TopRowCount\":\"0\",\"StartRow\":\"0\",\"Limit\":\"0\"}"
    //    ]
    //}));
    logger.info(JSON.stringify(queryBills));
}