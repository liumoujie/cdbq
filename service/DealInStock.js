/**
 * Created by zhoulanhong on 11/6/18.
 */
var fs = require('fs');
var conf = require('../config/cdbq_config');
var logger = require('../config/log4js').getLogger('DealInStock');
module.exports=function(billData, billType){
    "use strict";

    let getChineseName = function(vList) {
        for (let j in vList) {
            if (vList[j].Key == "2052") {
                //console.log("GYSNAME:" + vList[j].Value); //供应商名称
                return vList[j].Value;
            }
        }
        return "";
    }

    let queryRet = JSON.parse(billData);
    let retData = queryRet.Result.Result;
    let entries = retData.InStockEntry;

    logger.info(JSON.stringify(queryRet));

    let txtString = "";
    for (let i in entries) {
        console.log("----------------------------");
        console.log("GYSID:" + retData.DemandOrgId.Number); //需求组织ID
        txtString += retData.DemandOrgId.Number + ",";
        console.log("GYSNAME:" + getChineseName(retData.DemandOrgId.Name)); //需求组织名称
        txtString += getChineseName(retData.DemandOrgId.Name) + ",";

        console.log("RKRQ:" + retData.Date); //入库日期
        txtString += retData.Date.substring(0,10) + ",";

        console.log("GYSID:" + retData.SupplierId.Number); //供应商ID
        txtString += retData.SupplierId.Number + ",";
        console.log("GYSNAME:" + getChineseName(retData.SupplierId.Name)); //供应商名称
        txtString += getChineseName(retData.SupplierId.Name) + ",";

        console.log("RKDH:" + retData.BillNo); //入库单号
        txtString += retData.BillNo + ",";
        console.log("CGDDH:" + retData.FSRCBillNo); //采购订单号
        if(retData.FSRCBillNo){
            txtString += retData.FSRCBillNo + ",";
        }else{
            txtString += ",";
        }

        console.log("CKDM:" + entries[i].StockId.Number); //仓库代码
        txtString += entries[i].StockId.Number + ",";
        console.log("CKMC:" + getChineseName(entries[i].StockId.Name)); //仓库名称
        txtString += getChineseName(entries[i].StockId.Name) + ",";

        console.log("CKDM:" + entries[i].StockLocId.F100008.Number); //仓位代码
        txtString += entries[i].StockLocId.F100008.Number+ ",";
        console.log("CKMC:" + getChineseName(entries[i].StockLocId.F100008.Name)); //仓位名称
        txtString += getChineseName(entries[i].StockLocId.F100008.Name) + ",";

        console.log("WLBM:" + entries[i].MaterialId.Number); //材料代码
        txtString += entries[i].MaterialId.Number + ",";
        console.log("WLMC:" + getChineseName(entries[i].MaterialId.Name)); //材料名称
        txtString += getChineseName(entries[i].MaterialId.Name) + ",";

        console.log("SSSL:" + entries[i].RealQty); //实收数量
        txtString += entries[i].RealQty + ",";
        console.log("SSJE:" + entries[i].Amount); //实收金额
        txtString += entries[i].Amount + "\n";
    }

    let fileName = retData.BillNo + ".txt";
    let repPath = conf.downFileBase + "/public/report/" + billType + "/" + fileName;
    let downloadPath = "downloadFile/" + retData.BillNo;

    console.log("write data : "+txtString)

    let writeNbrs = fs.writeFileSync(repPath, txtString);

    return {
        fileName : fileName,
        repPath : repPath,
        downloadPath : downloadPath
    }
}