/**
 * Created by zhoulanhong on 11/6/18.
 */
let conf = require('../config/cdbq_config');
let request = require('sync-request');

var logger = require('../config/log4js').getLogger('SessionService');

module.exports=function(config){
    config = config || conf.loginInfo;
    let loginBack = request('POST', conf.baseUrl + conf.webApi.loginUrl
        , {
            json: {
                "parameters": [

                    "5bd01bc4ea5ec2",
                        "admin",
                        "cdbqtest",
                        "3964a021740e4ff08a00e6ba9fe0e335",
                        2052

                    //config.dbCenterId,
                    //config.userName, //"admin",
                    //config.appId,//"cdbqtest",
                    //config.appPassword,//"3964a021740e4ff08a00e6ba9fe0e335",
                    //config.lang //2052
                ]
            }
        });

    logger.info(JSON.stringify(conf.baseUrl + conf.webApi.loginUrl));
    //"5bd01bc4ea5ec2",
    //    "admin",
    //    "cdbqtest",
    //    "3964a021740e4ff08a00e6ba9fe0e335",
    //    2052
    logger.info(JSON.stringify({
        "parameters": [
            config.dbCenterId,
            config.userName, //"admin",
            config.appId,//"cdbqtest",
            config.appPassword,//"3964a021740e4ff08a00e6ba9fe0e335",
            config.lang //2052
        ]
    }));

    logger.info(loginBack.getBody('utf-8'));



    try {
        logger.info(loginBack.headers);
        let ckstr = JSON.stringify(loginBack.headers);
        ckstr = (ckstr.substring(ckstr.indexOf('kdservice-sessionid'))).split(";");
        logger.info("get session in session service : " + ckstr[0]);
        return ckstr[0];
    }catch(e){
        return null;
    }
}