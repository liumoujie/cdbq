/**
 * Created by zhoulanhong on 3/29/17.
 */
var allSelectedStaff = "";
var selStaffIndex = 0;
var selStaffId = 0;
var rep = new function () {

    var lineColors = ["#ff0000", "#00ffff", "#00aeef", "#fff200", "#05d221", "#ff530d", "#ff8b00", "#00ff48", "#0cf8b8", "#0133f8"];
    this.showFlag = false;
    this.localFlag = false;
    this.realTraceFlg = 0;
    this.realTimeSlected = "";
//用于保存每个用户对应的 html 数据,方便切换人员的时间,自动用生成好的html替换表格的内容
    this.replayPoints = new Map();
//用于保存每个用户对应的 所有轨迹点 数据,播放轨迹
    this.replayPointsForTrace = new Map();
//保存当前播放到了第几步,用于在手动播放的时候,控制步数
    this.currentStep = 0;
//自动播放轨迹时的定时器
    this.repInternal = null;
//自动播放轨迹时的播放步部
    this.repIndex = 0;
//人员选择idx计数
    this.selStaffIndex = 0;
//当前选择的人员的Id
    this.selStaffId = 0;
//设置当前是全部还是单人模式,默认是1:单人模式 0:查询出所有
    this.replayType = 1;
//重置搜索条件,重置之后,需要进行重新查询
    this.resetQueryCondition = 0;
//保存当前的开始时间,用于判断时间是否已经重新修改
    this.saveCurSt = null;
//保存当前的结束时间,用于判断时间是否已经重新修改
    this.saveCurEt = null;

    this.selectedStaffs = new Set();
    this.allSelectedStaff = "";
    var that = this;
    this.setSelectedStaffs = function (newSet) {
        this.selectedStaffs = newSet;
    }
    this.getSelectedStaffs = function () {
        return this.selectedStaffs;
    }

    this.setAllSelectedStaff = function (str) {
        this.allSelectedStaff = str;
    }
    this.getAllSelectedStaff = function () {
        return this.allSelectedStaff;
    }
    this.selectStaff = function (event, treeId, treeNode) {
        that.selectedStaffs = new Set();
        that.allSelectedStaff = "";
        allSelectedStaff = "";
        var treeObj = $.fn.zTree.getZTreeObj(treeId);
        var checkedNodes = treeObj.getCheckedNodes();
        var btnIdx = 0;
        var loggingIdx = 0;
        for (var i in checkedNodes) {
            // 不是叶子节点
            if (!checkedNodes[i].isParent) {
                //allSelectedStaff += checkedNodes[i].name + " ";
                generateStaffButton(checkedNodes[i].name, checkedNodes[i]._id, btnIdx);
                loggingStaffButton(checkedNodes[i].name, checkedNodes[i]._id, btnIdx);
                that.selectedStaffs.add(
                    {
                        "_id": checkedNodes[i]._id,
                        "idNumber": checkedNodes[i].idNumber,
                        "phoneNbr": checkedNodes[i].phoneNbr,
                        "staffName": checkedNodes[i].name
                    }
                );
                btnIdx++;
                loggingIdx++;
            }
        }

        showSelectCount(that.selectedStaffs.size);
        $('#loggingReplaySelectedStaff').html(allSelectedStaff);
        $('#replaySelectedStaff').html(that.allSelectedStaff);
        var choseNum = $('#replaySelectedStaff').find('input[type=button]').length;
        var firsrChilder = $('#replaySelectedStaff').children().first().val();
        $('.choseNum').html(choseNum);
        if (choseNum == 0) {
            $('#choiseZtree').html('请在组织树上选择').removeClass('replayTitle').addClass('nochose');
            $('.chisePeopel').html('');
        } else {
            $('#choiseZtree').removeClass('nochos').addClass('replayTitle');
            $('.chisePeopel').html(firsrChilder);
        }
    }
    function loggingStaffButton(nodeName, id, loggingIdx) {
        allSelectedStaff.length > 0 ?allSelectedStaff +=
            "&nbsp;&nbsp;<input type='button' id='btns_" + id + "' class='btn btn-default' onclick=\"rep.changeStaff('" + id + "','" + loggingIdx + "')\" value='" + nodeName + "'>" :
            allSelectedStaff = "<input type='button' id='btns_" + id + "' class='btn btn-primary'  onclick=\"rep.changeStaff('" + id + "','" + loggingIdx + "')\" value='" + nodeName + "'>";
    }
    function generateStaffButton(nodeName, id, btnIdx) {
        that.allSelectedStaff.length > 0 ? that.allSelectedStaff +=
            "&nbsp;&nbsp;<input type='button' id='btn_" + id + "' class='btn btn-default' onclick=\"rep.changeReplayStaff('" + id + "','" + btnIdx + "')\" value='" + nodeName + "'>" :
            that.allSelectedStaff = "<input type='button' id='btn_" + id + "' class='btn btn-primary'  onclick=\"rep.changeReplayStaff('" + id + "','" + btnIdx + "')\" value='" + nodeName + "'>";
    }

    this.changeReplayStaff = function (id, btnIdx) {
        //切换人员之后,重置所有的回放设置
        if (that.replayType == "0") {
            //如果是全部选择,那么重新选人,不需要修改查询条件,直接读取数据
            that.resetReplay(1);
        } else {
            that.resetReplay(0);
        }
        //先对button进行着色变换,标志当前是选择的哪个button(即哪名员工)
        if (btnIdx) {
            that.selStaffIndex = btnIdx;
            that.selStaffId = id;
        }
        var preSelect = $(".btn.btn-primary");
        preSelect.removeClass();
        preSelect.addClass("btn btn-default");
        $('#btn_' + id).removeClass();
        $('#btn_' + id).addClass("btn btn-primary");
        //显示当前选中的人员姓名
        var btnName = $('#btn_' + id).val();
        $('.chisePeopel').html(btnName);
        var repPoints = that.replayPoints.get(id);
        if (repPoints) {
            $('#tracedetailtable').html(repPoints);
        } else {
            if ($("#stDate").val() == "" || $("#edDate").val() == "") {
                $('#choiseZtree').html('请设置开始和结束时间').removeClass('replayTitle');
            }else {
                $('#tracedetailtable').html('<tr><td colspan="8" align="center">数据查询中...</td></tr>');
            }
        }
        //立即开发播放
        that.replay();
    }


    //当有一些需要重置
    //param 如果flg 是 1, 则无需更新resetQueryCondition
    this.resetReplay = function (flg) {
        if (flg != 1) {

            that.resetQueryCondition = 1;
        }
        //清除当前的播放
        if (that.repInternal) {

            clearInterval(that.repInternal);
            that.repInternal = null;
        }
        //当前播放步数重置
        that.currentStep = 0;
        //自动播放轨迹步数重置
        that.repIndex = 0;
        if (flg != 1) {

            //清除所有当前的轨迹对象
            m.clearOverlays();
        }
        //清除所有轨迹点
        $('#tracedetailtable').html('');
    }

    //自动播放的时候,暂停回放
    this.pause = function () {
        if (that.repInternal) {
            clearInterval(that.repInternal);
            that.repInternal = null;
        }
    }

    //轨迹回放
    this.replay = function () {
        //改变了 radio button , 也需要重置查询条件
        if (that.replayType != $('input:radio[name="checked"]:checked').val()) {
            that.replayType = $('input:radio[name="checked"]:checked').val();
            that.resetReplay();
        }
        var treeObj = gisOrgTree.getTree();
      var treePresonNum = treeObj.getCheckedNodes();

        if (that.selectedStaffs.size == 0 && treePresonNum.length<2) {
            $('#choiseZtree').html('请先选择要回放的人员').removeClass('replayTitle');
            return false;
        }
        var stDate = new Date($("#stDate").val());
        var edDate = new Date($("#edDate").val());
        if (stDate.getTime() > edDate.getTime()) {
            $('#choiseZtree').html('请正确设置时间').removeClass('replayTitle');
            return;
        }
        // 计算两个时间相差多少天
        var time = edDate.getTime() - stDate.getTime() ;
        var times = Math.floor(time/(24*60*60*1000));
        if (times > 4){
            $('#choiseZtree').html('请查询5日内轨迹').removeClass('replayTitle');
            return;
        }

        if ($("#stDate").val() == "" || $("#edDate").val() == "") {
            $('#choiseZtree').html('请设置开始和结束时间').removeClass('replayTitle');
            return;
        }

        //开始时间变更重置所有回放条件
        if (that.saveCurSt == null) {
            that.saveCurSt = $("#stDate").val();
        } else {
            if (that.saveCurSt != $("#stDate").val()) {
                that.saveCurSt = $("#stDate").val();
                that.resetReplay();
            }
        }
        //结束时间变更重置所有回放条件
        if (that.saveCurEt == null) {
            that.saveCurEt = $("#edDate").val();
        } else {
            if (that.saveCurEt != $("#edDate").val()) {
                that.saveCurEt = $("#edDate").val();
                that.resetReplay();
            }
        }

        var selStaffs = "";
        var uId = that.getCurrentSelStaff();
        if (uId) {
            var selStaffs = uId;
        }
        //说明这个用户已经查过了,并且没有改变过查询条件,直接播放就可以了.
        if (!that.resetQueryCondition && that.replayPoints.get(selStaffs) && that.replayPoints.get(selStaffs).length > 0) {
            if (that.replayPoints.get(selStaffs) == "noData") {
                $('#tracedetailtable').html('<tr><td colspan="8" align="center">当前时间段无数据</td></tr>');
            } else {
                that.beginReplay();
            }
        } else {
            //全部查询出来
            if (that.replayType == 0) {
                selStaffs = "";
                that.selectedStaffs.forEach(function (staff) {
                    selStaffs += selStaffs.length > 0 ? ("," + staff._id ) : staff._id;
                });
            }
            that.commonReplay(selStaffs, $("#stDate").val(), $("#edDate").val());
            //显示提示信息
            $('#tracedetailtable').html('<tr><td colspan="8" align="center">数据查询中...</td></tr>');
        }
    }

    //获取当前在播放的用户
    this.getCurrentSelStaff = function () {
        var preSelect = $(".btn.btn-primary");
        if (preSelect && preSelect.attr("id")) {
            var userIdStr = preSelect.attr("id").substring(4, preSelect.attr("id").length);
            var userId = userIdStr.replace("_","");
            return userId
        }
        return null;
    }


    /*
    * 日志点击查询
    * */
    this.loggingPlay = function () {
        var stDate = $('#loggingstDate').val();
        var edDate = $('#loggingedDate').val();
        var treeObj = gisOrgTree.getTree();
        var treePresonNum = treeObj.getCheckedNodes();
        var stTime = new Date($("#loggingstDate").val());
        var edTime = new Date($("#loggingedDate").val());
        var uId = that.getLoggingSelStaff(treePresonNum);
         //idNumber
        that.selectedStaffs.forEach(function (staff) {
            var preSelect = $(".btn.btn-primary");
            if (preSelect && preSelect.attr("id")) {
                preSelect.attr("id").substring(5, preSelect.attr("id").length) == staff._id;
                uId = staff.idNumber;
            }
        });

        if (stTime.getTime() > edTime.getTime()) {
            $('#choiseZtreeLogging').html('请正确设置时间').removeClass('replayTitle');
        }else if (stDate == '' || edDate == ''){
            $('#choiseZtreeLogging').html('请设置开始和结束时间').removeClass('replayTitle');
        }else if (that.selectedStaffs.size == 0 && treePresonNum.length<2){
            $('#choiseZtreeLogging').html('请先选择要回放的人员').removeClass('replayTitle');
         } else if (uId) {
             that.createLoggingData(uId,stDate,edDate);
        }else {
            $('#choiseZtreeLogging').html('请选择需要查询人员').removeClass('replayTitle');
        }
    }

    //获取当前日志记录点击的用户
    this.getLoggingSelStaff = function (treePresonNum) {
        var preSelect = $(".btn.btn-primary");
        var preSelectId = '';
        if (preSelect && preSelect.attr("id")) {
            preSelectId = preSelect.attr("id").substring(5, preSelect.attr("id").length) ;
        }else {
            return;
        }
        for (var i in treePresonNum){
            if (preSelectId == treePresonNum[i]._id){
                return treePresonNum[i].idNumber;
            }
        }
    }

    // 获取日志查询数据
    this.createLoggingData = function (uId,stDate,edDate) {
        $.get('/api/listChatRoomByUserIdAndDate?startDate=' + getTimeByDate(stDate) + '&endDate='
            + getTimeByDate(edDate) + '&userId=' + uId, function (data) {
            if (data && data.status == 200) {
                var datas = data.data;
                console.log("rep datas : " + JSON.stringify(datas));

            }else {
                $('#choiseZtreeLogging').html('当前无数据').removeClass('replayTitle');
            }

        });
    };


    /**
     * 轨迹回放抽象出来的一个公共类
     * @param slecteStaffs 需要回放轨迹的人员,用逗号分开的userid(不是idNumber)
     * @param startDate 回放开始时间
     * @param endDate 回放结束时间
     * */

    this.commonReplay = function (slecteStaffs, startDate, endDate) {
        //重新查询的时候,先清空以前的数据
        that.replayPoints = new Map();
        that.replayPointsForTrace = new Map();
        //重置查询条件
        that.resetQueryCondition = 0;
        //先清空所有以前的回放数据,包括地图上的点和线以线地址列表
        //map.clearAllReplayObjects();

        //console.log('/api/queryLocs?startTime=' + getTimeByDate(startDate) + '&endTime='
        //    + getTimeByDate(endDate) + '&userId=' + slecteStaffs);
        //startDate = "2017-02-22 08:40";
        //endDate = "2017-02-22 09:40";
       // var userId = slecteStaffs.replace("_","");
        $.get('/api/queryLocs?startTime=' + getTimeByDate(startDate) + '&endTime='
            + getTimeByDate(endDate) + '&userId=' + slecteStaffs, function (data) {
            //$.get('/api/queryLocs?startTime=1485957666211&endTime=1487080806211&userId=57993d998920862b267a63ef,57993f50fbd722c7265211dc,579a1558ee41cc10105a169b,58953b97a23581499f0fb1c0', function (data) {
            if (data && data.status == 200) {
                var datas = data.data;
                console.log("rep datas : " + JSON.stringify(datas))
                if (datas.length > 0) {
                    //先找出有多少人
                    var users = _.uniqBy(datas, function (item) {
                        return item.userId;
                    });

                    //没有数据的员工,先补足noData的空位
                    that.selectedStaffs.forEach(function (staff) {
                        var needAdd = true;
                        for (var j in users) {
                            if (staff._id == users[j].userId) {
                                needAdd = false;
                            }
                        }
                        if (needAdd) {
                            that.replayPoints.set(staff._id, "noData");
                        }
                    });


                    var userIds = "";
                    for (var uid in users) {
                        userIds += userIds.length > 0 ? ",'" + users[uid].userId + "'" : "'" + users[uid].userId + "'";
                    }

                    var addressPromises = [];
                    var signles = "";
                    var groupDatas = [];
                    for (var u in users) {
                        var curDatas = _.filter(datas, function (item) {
                            return item.userId == users[u].userId;
                        });
                        groupDatas[u] = curDatas;
                        addressPromises.push(Promise.map(curDatas, function (p) {
                            return m.getAddressByLoc(p.lng, p.lat);
                        }, {concurrency: 40}));
                    }
                    queryAddress(addressPromises, users, groupDatas);
                    $('#choiseZtree').html("请在组织树上选择").addClass('replayTitle');
                } else {
                    $('#tracedetailtable').html('<tr><td colspan="8" align="center">当前时间段无数据</td></tr>');

                    $('#choiseZtree').html("没有查询到轨迹信息").removeClass('replayTitle');
                }
            } else {
                $('#choiseZtree').html("查询轨迹信息出错,请重新查询").removeClass('replayTitle');
                $('#tracedetailtable').html('<tr><td colspan="8" align="center">查询轨迹信息出错,请重新查询</td></tr>');
            }
        });
    }

    function queryAddress(addressPromises, users, groupDatas) {
        Promise.all(addressPromises).then(function (data) {
            //将相关的数据都设置到对应的set里面
            for (var i in data) {
                signles = "";
                var tmpi = data[i];
                var curbaseInfo = getPhoneAndNameForReplay(users[i].userId);

                var idx = 1;
                for (var j = 0; j < tmpi.length; j++) {

                    var nextPoint = null;
                    if (j + 1 < tmpi.length) {
                        nextPoint = {
                            lng: tmpi[j + 1].point.lng,
                            lat: tmpi[j + 1].point.lat
                        }
                    }
                    m.markPoints({
                            idx: users[i].userId + "" + j,
                            repTime: groupDatas[i][j].repTime,
                            name: curbaseInfo.name,
                            phone: curbaseInfo.phone,
                            speed: users[i].speed,
                            address: tmpi[j].address,
                            lng: tmpi[j].point.lng,
                            lat: tmpi[j].point.lat,
                            remmber: j + 1
                        }, nextPoint,
                        null, lineColors[i]
                    );

                    signles += '<tr name="' + groupDatas[i][j].repTime + '" id="' + users[i].userId + "" + j + '" ' +
                        'onclick="loctionFocus(\'' + (users[i].userId + "" + j) + '\')">' +
                        '<td style="border-radius:0px; border-left:none; width: 5%">' + (idx++) + '</td>' +
                        '<td style="width: 18%">' + groupDatas[i][j].repTime + '</td>' +
                        '<td style="width: 8%">' + curbaseInfo.name + '</td>' +
                        '<td style="width: 10%">' + curbaseInfo.phone + '</td>' +
                        '<td style="width: 12%">' + users[i].speed + '</td>' +
                        '<td style="width: 27%">' + tmpi[j].address + '</td>' +
                        '<td style="width: 10%">' + tmpi[j].point.lng + '</td>' +
                        '<td style="width: 10%">' + tmpi[j].point.lat + '</td>' +
                        '</tr>';
                }
                that.replayPoints.set(users[i].userId, signles);
                that.replayPointsForTrace.set(users[i].userId, tmpi);

            }
            that.beginReplay();

        }).catch(function (e) {
            alert(e);
        });

    }


    //轨迹导出报表
    this.exprotReport = function () {

        if ($("#stDate").val() == "" || $("#edDate").val() == "") {

            $('#choiseZtree').html('请设置开始和结束时间').removeClass('replayTitle');
            return;
        }

        var selStaffs = "";
        var selStaffNams = "";
        var preSelect = $(".btn.btn-primary");
        if (preSelect && preSelect.attr("id")) {
            selStaffNams = preSelect.attr("value");
            var selStaffs = preSelect.attr("id").substring(4, preSelect.attr("id").length);
        }

        //全部查询出来
        if (that.replayType == 0) {
            selStaffs = "";
            selStaffNams = "";
            that.selectedStaffs.forEach(function (staff) {
                selStaffs += selStaffs.length > 0 ? ("," + staff._id ) : staff._id;
                selStaffNams += selStaffNams.length > 0 ? ("," + staff.staffName ) : staff.staffName;
            });
        }

        $.get('/api/exportLocs?startTime=' + getTimeByDate($("#stDate").val()) + '&endTime='
            + getTimeByDate($("#edDate").val()) + '&userId=' + selStaffs + '&names=' + selStaffNams, function (data) {
            //$.get('/api/queryLocs?startTime=1482355501441&endTime=1482417601441&userId=57c44a100e9657946a87e3d6,57c44a240e9657946a87e3d7', function (data) {
            if (data && data.status == 200) {
                window.open(data.data);
            } else {
                $('#choiseZtree').html('报表导出错误').removeClass('replayTitle');
            }
        });
    };

    this.strechReplay = function () {
        $('#rePlay').css({
            top: '0px',
            bottom: '0px'
        });

        $('#rePlay').show();
        //$('.shirinkBtn').slideDown("slow");
        $('.authorize').hide();
    }


    this.beginReplay = function () {

        if (that.repInternal) {
            //如果正在回放,直接返回
            return false;
        }

        var uId = that.getCurrentSelStaff();
        if (uId) {
            var sigles = that.replayPoints.get(uId) ? that.replayPoints.get(uId) :
                '<tr><td colspan="8" align="center">当前时间段无数据</td></tr>';
            $('#tracedetailtable').html(sigles);
            var tmpi = that.replayPointsForTrace.get(uId);
            //默认将回放的图标设置到第一个人的第一个点,如果是有第二个点,同时加上方向
            if (tmpi && tmpi.length > 1) {
                that.autoReplayTrace();
            }

        }
    }


    this.autoReplayTrace = function () {
        that.repIndex = 0;
        //先对button进行着色变换,标志当前是选择的哪个button(即哪名员工)
        var uId = that.getCurrentSelStaff();
        if (uId) {
            var points = that.replayPointsForTrace.get(uId);
            if (points && points.length > 1) {
                var replayPoints = [];
                for (var p = that.currentStep; p < points.length - 1; p++) {
                    var addOtherPoints = insertPixelsForLong(
                        {lng: points[p].point.lng, lat: points[p].point.lat},
                        {lng: points[p + 1].point.lng, lat: points[p + 1].point.lat});
                    for (var j = 0; j < addOtherPoints.length - 1; j++) {
                        var middleP1 = m.pixelToPoint(addOtherPoints[j].x, addOtherPoints[j].y);
                        var middleP2 = m.pixelToPoint(addOtherPoints[j + 1].x, addOtherPoints[j + 1].y);

                        replayPoints.push({
                            "p1": middleP1,
                            "p2": middleP2,
                            "idx": uId + "" + (that.currentStep + 1),
                            "repIdx": that.currentStep
                        });
                    }
                    that.currentStep++;
                }
                setIntervalForRun(replayPoints, uId);

            } else {
                //alert("in    else");
            }

        } else {

            $('#choiseZtree').html("请选择需要回放轨迹的人员").removeClass('replayTitle');
        }
    }

    //设置回放的时间间隔
    function setIntervalForRun(replayPoints, uId) {
        that.repInternal = setInterval(function () {

            if (that.repIndex < replayPoints.length - 1) {

                hightCurrentTr(uId, that.currentStep);

                jumpStep(replayPoints[that.repIndex].p1, replayPoints[that.repIndex].p2, replayPoints[that.repIndex].idx);
                //根据当前的点,设置回放步数,在暂停的时候,可以重新回到对应的点上;
                that.currentStep = replayPoints[that.repIndex].repIdx;
                that.repIndex++;
            } else {
                //播放完成之后,回到第一步
                that.currentStep = 0;
                clearInterval(that.repInternal);
                that.repInternal = null;
            }
        }, 30);
    }

    //通过id在树上获取电信号码和名字
    function getPhoneAndNameForReplay(id) {
        var allNodes = gisOrgTree.getAllNodes();
        for (var i in allNodes) {
            if (i == 0) {
                console.log(id + "  " + JSON.stringify(allNodes[i]));
            }
            if (allNodes[i]._id == id) {
                return {phone: allNodes[i].phoneNbr, name: allNodes[i].name};
            }
        }
        return {phone: 'undefined', name: 'undefined'};
    }


    //高亮并且自动滚动 tr
    function hightCurrentTr(uId, id) {
        var trId = uId + "" + id;
        $('.highLigthTr').removeClass("highLigthTr");
        $('#' + trId).addClass("highLigthTr");

        var tb = $('.table_title');
        tb.scrollTop(id * 29);
    }

    //选择上一个人
    this.selPreStaff = function () {
        var staffArray = [];
        that.selectedStaffs.forEach(function (staff) {
            staffArray.push(staff._id);
        });
        if (staffArray.length == 0) {
            $('#choiseZtree').html('当前无人员').removeClass('replayTitle');
            return false;
        }
        if (that.selStaffIndex == 0) {
            $('#choiseZtree').html('已经是第一个人了').removeClass('replayTitle');
            return false;
        } else {
            that.selStaffIndex--;
            that.changeReplayStaff(staffArray[that.selStaffIndex]);
        }
    }
    // 日志查询上一人
    this.loggingPreStaff = function () {
        var staffArray = [];
        that.selectedStaffs.forEach(function (staff) {
            staffArray.push(staff._id);
        });
        if (staffArray.length == 0) {
            $('#choiseZtreeLogging').html('当前无人员').removeClass('replayTitle');
            return false;
        }
        if (that.selStaffIndex == 0) {
            $('#choiseZtreeLogging').html('已经是第一个人了').removeClass('replayTitle');
            return false;
        } else {
            that.selStaffIndex--;
            that.changeStaff(staffArray[that.selStaffIndex]);
        }
    }
    //日志查询下一个人
    this.loggingNextStaff = function () {
        var staffArray = [];
        that.selectedStaffs.forEach(function (staff) {
            staffArray.push(staff._id);
        });
        if (staffArray.length == 0) {
            $('#choiseZtreeLogging').html('当前无人员').removeClass('replayTitle');
            return false;
        }
        if (that.selStaffIndex >= staffArray.length - 1) {
            $('#choiseZtreeLogging').html('已经是最后一个人了').removeClass('replayTitle');
        } else {
            that.selStaffIndex++;
            that.changeStaff(staffArray[that.selStaffIndex]);
        }
    }
    this.changeStaff=function(id,loggingIdx) {
        if (loggingIdx) {
            selStaffIndex = loggingIdx;
            selStaffId = id;
        }
        var preSelect = $(".btn.btn-primary");
        preSelect.removeClass();
        preSelect.addClass("btn btn-default");
        $('#btns_' + id).removeClass();
        $('#btns_' + id).addClass("btn btn-primary");
    }

    //跳到对应的步数上
    function jumpStep(from, to, infoIdx) {
        var pixel = m.pointToPixels(from.lng, from.lat);
        var nextPixel = m.pointToPixels(to.lng, to.lat);
        var ang = calcAngle(pixel, nextPixel);

        m.locateReplayIcon(to, "/images/navigation.png", infoIdx, ang);
    }

    //选择下一个人
    this.selNextStaff = function () {
        var staffArray = [];
        that.selectedStaffs.forEach(function (staff) {
            staffArray.push(staff._id);
        });
        if (staffArray.length == 0) {
            $('#choiseZtree').html('当前无人员').removeClass('replayTitle');
            return false;
        }
        if (that.selStaffIndex >= staffArray.length - 1) {
            $('#choiseZtree').html('已经是最后一个人了').removeClass('replayTitle');
        } else {
            that.selStaffIndex++;
            that.changeReplayStaff(staffArray[that.selStaffIndex]);
        }
    }

    //单步显示轨迹前一步
    this.preRaceStep = function () {
        this.pause();
        if (that.currentStep == 0) {
            $('#choiseZtree').html("当前已经是第一步了").removeClass('replayTitle');
            return null;
        } else {
            var uId = that.getCurrentSelStaff();
            if (uId) {
                var points = that.replayPointsForTrace.get(uId);
                var preRaceNum = that.currentStep - 1;
                var $objTr = $('#tracedetailtable tr[id="' + uId + preRaceNum + '"]'); //找到要定位的地方  tr
                $objTr.css('background', '#d03030').css('color', 'white').siblings().css('background', 'white').css('color', '#333'); //设置要定位地方的css
                var objTr = $objTr[0]; //转化为dom对象
                $(".table_title").animate({scrollTop: objTr.offsetTop - 47}, "slow"); //定位
                if (points && points.length > 1) {
                    hightCurrentTr(uId, that.currentStep);
                    jumpStep({lng: points[that.currentStep].point.lng, lat: points[that.currentStep].point.lat},
                        {lng: points[that.currentStep - 1].point.lng, lat: points[that.currentStep - 1].point.lat},
                        uId + "" + (that.currentStep - 1));
                    that.currentStep--;
                    m.panTo(points[that.currentStep].point.lng, points[that.currentStep].point.lat);
                    $('#choiseZtree').html("当前到了第" + (that.currentStep + 1) + "步").removeClass('replayTitle');
                }
            } else {

                $('#choiseZtree').html("请选择要回放的人员").removeClass('replayTitle');
                return null;
            }
        }
    }

    //单步显示轨迹后一步
    this.nextRaceStep = function () {
        this.pause();
        var uId = that.getCurrentSelStaff();
        if (uId) {
            var points = that.replayPointsForTrace.get(uId);
            var nextRaceNum = that.currentStep + 1;
            var $objTr = $('#tracedetailtable tr[id="' + uId + nextRaceNum + '"]'); //找到要定位的地方  tr
            $objTr.css('background', '#d03030').css('color', 'white').siblings().css('background', 'white').css('color', '#333'); //设置要定位地方的css
            var objTr = $objTr[0]; //转化为dom对象
            $(".table_title").animate({scrollTop: objTr.offsetTop - 47}, "slow"); //定位

            if (points && points.length > 1) {
                if (that.currentStep == points.length - 1) {
                    $('#choiseZtree').html("已经到了最后一步").removeClass('replayTitle');
                    return;
                }
                else {
                    hightCurrentTr(uId, that.currentStep);
                    jumpStep({lng: points[that.currentStep].point.lng, lat: points[that.currentStep].point.lat},
                        {lng: points[that.currentStep + 1].point.lng, lat: points[that.currentStep + 1].point.lat},
                        uId + "" + (that.currentStep + 1));
                    that.currentStep++;
                    m.panTo(points[that.currentStep].point.lng, points[that.currentStep].point.lat);
                    $('#choiseZtree').html("当前到了第" + (that.currentStep + 1) + "步").removeClass('replayTitle');
                }
            }
        } else {

            $('#choiseZtree').html("请选择要回放的人员").removeClass('replayTitle');
            return null;
        }
    }

    //在弹出的窗口中点击的轨迹回放
    this.winReplay = function(id, name) {
        $("#stDate").val('');
        $("#edDate").val('');
        $('#tracedetailtable').text('');
        $("#organization").show();
        $("#commandModeDiv").hide();
        $('#rePlay').show();
        $('.shirinkBtn').show();
        $('.stretchBtn').hide();
        $('.authorize').hide();
        $('#replaySelectedStaff').html("<input type='button' id='btn_" + id + "' class='btn btn-primary' onclick=\"changeReplayStaff('" + id + "')\" value='" + name + "'>");
        var firsrChilder = $('#replaySelectedStaff').children().first().val();
        $('#choiseZtree').removeClass('nochos').addClass('replayTitle');
        $('.chisePeopel').html(firsrChilder);
        var choseNum = $('#replaySelectedStaff').find('input[type=button]').length;
        $('.choseNum').html(choseNum);
        this.commonReplay(id, stGmt, edGmt);
    }

// 地图点击人物开启对讲
    this.phoneCtrl = function(type,user) {
        var phoneNumbers = [];
        var treeObj = gisOrgTree.getTree();
        var node = treeObj.getNodes();
         var changePerson = treeObj.transformToArray(node);
       console.log(changePerson);
        if(type == 1 || type == 2 || type == 4){
            changePerson.forEach(function (staff) {
                if (staff.name == user && staff.phoneNbr !=null){
                    phoneNumbers.push(staff.phoneNbr);
                }else {
                    return;
                }
            });
            pttChatClient.maximum();
        }
        authenticate(function testCa() {
            pttChatClient.call(phoneNumbers, type);
        });
    }

}();