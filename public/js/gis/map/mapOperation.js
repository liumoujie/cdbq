/**
 * Created by zhoulanhong on 12/16/16.
 * 本文件定义所有将会在地图上进行的操作,具体的操作实现,将由不同的地图有不同的实现
 * 所有异步方法,如根据坐标调地址等.提供回调函数和bulebird两种调用格式
 */

function MapOperation(map) {

    if (!map) {
        throw new Error("map is necessary !!!");
    }

    this.map = map;

    //返回当前map
    this.getMap = function (startX, startY) {
        this.map.getMap(startX, startY);
    }


    this.getMapObj = function () {
        return this.map.getMapObj();
    }

    /*
     * 初如化地图
     *
     * @param overview 是否需要显示类型和缩略图
     * @param geolocation 是否需要定位的导航控件
     * */
    this.initMap = function (divId, overview, geolocation) {
        map.initMap(divId, overview, geolocation);
    };
    /*
     * 定位,通过经纬度将位置显示到地图上的对应位置
     *
     * @param lat 经度
     * @param lng 纬度
     * @param speed 速度
     * @param iconPath 显示的图标
     * @param name 名字
     * @param userid 用户idNumber
     * @param id 用户id
     * @param lastLocationTime 最后上报位置的时间
     * @param position 职位
     * @param locType 0:单人定位; 1:群体定位
     * */
    this.locate = function (userObj, locType) {
        map.locate(userObj, locType);
    };

    /*
     *轨迹回放之前先把所有的点和连线显示到地图上
     *
     * @param replayPoints    轨迹点的相关信息
     * @param icon      图标
     * @param lineColor 轨迹颜色
     * */
    this.markPoints = function (replayPoints, icon, lineColor) {
        map.markPoints(replayPoints, icon, lineColor);
    };

    /*
     *轨迹回放
     *
     * @param locations 坐标数组
     * @param icon      图标
     * @param lineColor 轨迹颜色
     * @param id        文字span的id
     * */
    this.replay = function (locations, icon, lineColor, id) {
        map.replay(locations, icon, lineColor, id);
    };

    /*
     *逆地址解析
     *
     * @param lat
     * @param lng
     * @param func 回调的地址函数,如果此参数为空,则返回bluebird的promise的对象
     * */
    this.getAddressByLoc = function (lat, lng, func) {
        if (typeof func == "function") {
            map.getAddressByLoc(lat, lng, func);
        }
        else {
            return map.getAddressByLoc(lat, lng);
        }
    };

    /*
     *通过地图,获取当前位置
     *
     * @param func 回调的地址函数,如果此参数为空,则返回bluebird的promise的对象
     * */
    this.getCurLocation = function (func) {
        if (typeof func == "function") {
            map.getCurLocation(func);
        }
        else {
            return map.getCurLocation();
        }
    };

    /*
     *通过div上选择的坐标位置,来确定这个位置对应的经纬度范围
     *@para x 坐标的x点
     *@para y 从标的y点
     * */
    this.pixelToPoint = function (x, y) {
        return map.pixelToPoint(x, y);
    }

    /*
     *通过地图上所有的点,算出中心位置以及地图要缩放的比例
     *@para points 坐标数组,
     *@para defaultResult 默认的中心位置和缩放比例
     *@return 中心点和缩放比例 {center:{x:12,y:23},zoom:11}
     * */
    this.getCenterAndZoom = function (points, defaultResult) {
        return map.getCenterAndZoom(points, defaultResult);
    }

    /*
     *关闭地图上打开的信息窗口
     * */
    this.closeInfoWindow = function () {
        return map.closeInfoWindow();
    }

    /*
     *清除掉所有的轨迹回放加入的点
     * */
    this.clearAllReplayObjects = function () {
        return map.clearAllReplayObjects();
    }

    /*
     *通过指定的ID,显示地图上某一点的信息
     * idx
     * */
    this.locateByReplayId = function (idx) {
        return map.locateByReplayId(idx);
    }

    /*
     *通过指定的ID,在地图上显示回放的动画效果
     * @param userId 需要回放的用户ID
     * @param step   回放的步骤, 如果此参数有值,代表是单步播放,如此参数为2,则显示1-->2的轨迹
     * */
    this.replayTrace = function (userId, step) {
        return map.replayTrace(userId, step);
    }

    /*
     *通过地图上经纬度的图标,获取点在地图上的位置
     * @param lng 需要转换的点的经度
     * @param lat 需要转换的点的纬度
     * @return pixels 转换出来之后,页面上的位置
     * */
    this.pointToPixels = function (lng, lat) {
        return map.pointToPixels(lng, lat);
    }

    /*
     *在地图上显示回放轨迹的图标
     * @param point 需要转换的点的纬度
     * @param icon 转换出来之后,页面上的位置
     * @param infoIdx 显示窗口的id
     * @param rotation 图标需要旋转的角度
     * */
    this.locateReplayIcon = function (point, icon, infoIdx, rotation) {
        return map.locateReplayIcon(point, icon, infoIdx, rotation);
    }

    /*
     *清除地图上所有的覆盖物
     *
     * */
    this.clearOverlays = function (obj) {
        map.clearOverlays(obj);
    }
    /*

     /**
     * 过经纬度将标记显示到地图上的对应位置
     */
    this.markLocate = function (markObject) {
        map.markLocate(markObject);
    }

    /**
     * 过经纬度将标记显示到地图上的对应位置
     */
    this.markSquareLocate = function (markObject) {
        map.markSquareLocate(markObject);
    }

    /**
     * 通过经纬度将标记显示到地图上的对应位置,画背景
     */
    this.markBGSquareLocate = function (markObject) {
        map.markBGSquareLocate(markObject);
    }

    /**
     * 显示不同的点,同时定位中心
     */
    this.showAllSatations = function (points) {
        map.showAllSatations(points);
    }

    /**
     * 过经纬度将标记显示到地图上的对应位置
     */
    this.markPathLocate = function (markObject) {
        map.markPathLocate(markObject);
    }


    this.markAllSatations = function (markObject) {
        map.markAllSatations(markObject);
    }

    this.markLoggingSatations = function (markObject) {
        map.markLoggingSatations(markObject);
    }

    /**
     * 通过经纬度定位
     */
    this.centerAndZoom = function (lng, lat) {
        map.centerAndZoom(lng, lat);
    }

    /**
     * 地图平移定位
     * */
    this.panTo = function (lng, lat) {
        map.panTo(lng, lat);

    }

}