/**
 * Created by zhoulanhong on 3/29/17.
 */

var dataTablesLang = {
    "sProcessing": "处理中...",
    "sLengthMenu": "显示 _MENU_ 项结果",
    "sZeroRecords": "没有匹配结果",
    "sInfo": "显示第 _START_ 至 _END_ 项结果，共 _TOTAL_ 项",
    "sInfoEmpty": "显示第 0 至 0 项结果，共 0 项",
    "sInfoFiltered": "(由 _MAX_ 项结果过滤)",
    "sInfoPostFix": "",
    "sSearch": "搜索:",
    "sUrl": "",
    "sEmptyTable": "表中数据为空",
    "sLoadingRecords": "载入中...",
    "sInfoThousands": ",",
    "oPaginate": {
        "sFirst": "首页",
        "sPrevious": "上页",
        "sNext": "下页",
        "sLast": "末页"
    },
    "oAria": {
        "sSortAscending": ": 以升序排列此列",
        "sSortDescending": ": 以降序排列此列"
    }
};

var planMgtTable = null;
var curPageSelected = [];
var realSelected = [];

$(document).ready(function () {
    $('#closeplanMgtBox').click(function () {
        $('.planMgtBox').hide();
        planMgtTable.destroy();
    });

    $('#selCloseImgMgt').click(function () {
        $('.selImgMgtBox').hide();
    });

    $('#viewCloseImgMgt').click(function () {
        $('.viewImgMgtBox').hide();
    });

    //添加预案
    $('#addPlanMgtBox').on('click', function (event) {
        event.preventDefault();
        //
        //alert($('.file-caption-name'));

        //$('#uploadFileName').val('只支持PDF格式'),
        //$('#showTdText').html('<div id="showFileUp"><input type="file" id="fileUp" name="fileUp" ' +
        //    'class="file-loading" data-show-preview="false"></div> <span style="display: none" id="showFileName"></span>');
        $('.file-caption-name').text('只支持PDF格式');


        $('#planName').val('');
        $('#planDesc').val('');

        $('#showFileUp').show();
        $('#showFileName').hide();

        //弹出对话框
        $('#dlgAddPlanMgt').modal({
            backdrop: 'static'
        });
    });


    $('#savePlanImgRel').on('click', function (evevt) {
        event.preventDefault();
        realSelected = [];
        $("#imageChbox:checked").each(function () {
            realSelected.push($(this).val());
        });

        if (realSelected.length == 0) {
            dialogAlert("操作结果", "没关联任何图件", "确定");
        }

        let dataJson = {
            "docId": $('#curDocId').val(),
            "curPageSelected": curPageSelected,
            "realSelected": realSelected
        }

        $.ajax({
            url: '/hfxt/planRelImg',
            dataType: 'json',
            type: 'post',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(dataJson),
            success: function (data) {
                //成功
                if (data.status == 200) {
                    $('#btnPlanMgtSave').text("保存");
                    $('#btnPlanMgtSave').prop('disabled', false);

                    dialogAlert("操作结果", "关联成功", "确定");

                    $('.selImgMgtBox').hide();
                    //重新加载table数据.
                    planMgtTable.ajax.reload();
                } else {
                    //显示错误提示信息
                    $('#dlgWaterPersonFormInputInvalid').text(data.message);
                    $('#dlgWaterPersonFormInputInvalid').removeClass('hidden');

                    $('#btnWaterPersonSave').text("保存");
                    $('#btnWaterPersonSave').prop('disabled', false);
                }
            },
            error: function (err) {
                location.href = '/';
            }
        });
    });


    //对话框点击保存
    $('#btnPlanMgtSave').on('click', function (event) {
            event.preventDefault();

            //有效性验证
            var planName = $('#planName').val();
            var planDesc = $('#planDesc').val();

            //输入合法
            if (planName) {

                $('#btnPlanMgtSave').text("数据保存中...");
                $('#btnPlanMgtSave').prop('disabled', true);

                var url = '';
                var dataJson = null;
                if ($('#planMgtSaveMode').val().length == 0) {
                    url = '/hfxt/planMgt';
                    dataJson = {
                        planName: planName,
                        planDesc: planDesc,
                        uploadFileName: $('#uploadFileName').val(),
                        uploadFileExtName: $('#uploadFileExtName').val(),
                        uploadFilePath: $('#uploadFilePath').val()
                    }
                } else {
                    url = '/hfxt/planMgtUpd';
                    dataJson = {
                        id: $('#planMgtSaveMode').val(),
                        planName: planName,
                        planDesc: planDesc
                    }
                }

                $.ajax({
                    url: url,
                    dataType: 'json',
                    type: 'post',
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(dataJson),
                    success: function (data) {
                        //成功
                        if (data.status == 200) {
                            $('#btnPlanMgtSave').text("保存");
                            $('#btnPlanMgtSave').prop('disabled', false);
                            $('#dlgAddPlanMgt').modal('hide');
                            //重新加载table数据.
                            planMgtTable.ajax.reload();
                        } else {
                            //显示错误提示信息
                            $('#dlgWaterPersonFormInputInvalid').text(data.message);
                            $('#dlgWaterPersonFormInputInvalid').removeClass('hidden');

                            $('#btnWaterPersonSave').text("保存");
                            $('#btnWaterPersonSave').prop('disabled', false);
                        }
                    },
                    error: function (err) {
                        location.href = '/';
                    }
                });
            }
            else {
                //输入不合法
                if (!planName) {
                    $('#dlgPlanMgtFormInputInvalid').text("预案名称不能为空");
                }
                $('#dlgPlanMgtFormInputInvalid').removeClass('hidden');
            }
        }
    );


    //导入用户按钮响应
    $('#fileUp').fileinput({
        language: 'zh',
        allowedFileExtensions: ['pdf'],//接收的文件后缀
        uploadUrl: '/upload/do/binary',
        uploadAsync: false
    });

    $('#fileUp').on('filebatchuploadsuccess', function (event, data, prev) {
        if (data.response.status == '200') {

            $('#uploadFileName').val($('#fileUp').val().replace(/^.+?\\([^\\]+?)(\.[^\.\\]*?)?$/gi, "$1"));
            $('#uploadFileExtName').val($('#fileUp').val().replace(/.+\./, ""));
            $('#uploadFilePath').val(data.response.data.fp);
            $('#fileUpTd').html($('#fileUp').val().replace(/^.+?\\([^\\]+?)(\.[^\.\\]*?)?$/gi, "$1") + $('#fileUp').val().replace(/.+\./, ""));

            $('#showFileUp').hide();
            $('#showFileName').html($('#fileUp').val().replace(/^.+?\\([^\\]+?)(\.[^\.\\]*?)?$/gi, "$1") + "." + $('#fileUp').val().replace(/.+\./, ""));
            $('#showFileName').show();

        } else {
            dialogAlert("操作结果", data.response.message, "确定");
            //$('#impFile').hide();
            //showImport = false;
        }
    });


});

//编辑群
function planMgtEdit(data) {
    $('#planMgtSaveMode').val(data.id);
    $('#planName').val(data.name);
    $('#planDesc').val(data.docDesc);
    //$('#fileUpTd').html(data.fileName);

    $('#showFileUp').hide();
    $('#showFileName').html(data.fileName);
    $('#showFileName').show();

    //弹出对话框
    $('#dlgAddPlanMgt').modal({
        backdrop: 'static'
    });

}

// 删除防汛负责人
function planMgtRemove(data) {

    dialogConfirm("请确认", "确定要删除本条数据？", function (result) {
        if (result) {
            $.ajax({
                url: '/hfxt/planMgtDel',
                dataType: 'json',
                type: 'post',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({
                    id: data.id
                }),
                success: function (data) {
                    //成功
                    if (data.status == 200) {
                        //重新加载table数据.
                        planMgtTable.ajax.reload();
                    } else {
                        //显示错误提示信息
                        dialogAlert("操作结果", data.message, "确定");
                    }
                },
                error: function (err) {
                    alert(JSON.stringify(err));
                    //location.href = '/';
                }
            });
        }
    });

}

function planMgtRelImg(data) {
    $('#curDocId').val(data.id);
    if ($('.selImgMgtBox').is(':hidden')) {
        //alert('--abc--')
        queryRelImgMgt('', '', '', 1);
        $('.selImgMgtBox').show();
    } else {
        return false;
    }
}

function queryViewRelImgMgt(page) {

    $.getJSON('/hfxt/viewPlanRelMgt', {
        page: page,
        docid: $('#curDocId').val()
    }, function (json) {

        relViewCreateTable(json.data.rows);

        if (json.data.cnt > 0) {
            $('.viewImgM-box').pagination({
                totalData: json.data.cnt,
                showData: 10,
                current: page,
                callback: function (api) {
                    queryViewRelImgMgt(api.getCurrent());
                }
            });
        } else {
            $('.imgM-box').html('');
        }
    });
}


function relViewCreateTable(data) {
    let tableHtml = '';
    let imgTrHtml = '<tr>';
    let textTrHtml = '<tr>';
    for (let i = 0; i < data.length; i++) {

        imgTrHtml += '<td width="20%"><img width="160" height="160" onclick="showBigImg(\'' + data[i].url + '\')" src="' + data[i].thumbnail + '"></td>';
        textTrHtml += '<td width="20%"><span>' + data[i].name + '</span></td>';
        if (i == 4) {
            imgTrHtml += '</tr>';
            textTrHtml += '</tr>';
            tableHtml = imgTrHtml + textTrHtml;
            imgTrHtml = '<tr>';
            textTrHtml = '<tr>';
        }
    }

    if (data.length < 4) {
        for (let j = 0; j < (5 - data.length); j++) {
            imgTrHtml += '<td width="20%">&nbsp;</td>';
            textTrHtml += '<td width="20%">&nbsp;</td>';
        }
    }

    imgTrHtml += '</tr>';
    textTrHtml += '</tr>';
    tableHtml += imgTrHtml + textTrHtml;

    if (data.length == 0) {
        tableHtml = '<tr><td>当前条件下无数据</td></tr>'
    }

    $('#viewImgMgtTable').html(tableHtml);
}


function queryRelImgMgt(type, wpName, nameKey, page, selIds) {

    $.getJSON('/hfxt/planRelImgMgt', {
        page: page,
        wpName: wpName,
        imgType: type,
        name: nameKey,
        docid: $('#curDocId').val()
    }, function (json) {

        relCreateTable(json.data.rows, json.data.relRows);
        if (json.data.cnt > 0) {
            $('.selImgM-box').pagination({
                totalData: json.data.cnt,
                showData: 10,
                current: page,
                callback: function (api) {
                    queryRelImgMgt(type, wpName, nameKey, api.getCurrent());
                }
            });
        } else {
            $('.imgM-box').html('');
        }
    });
}

function relCreateTable(data, selIds) {
    let tableHtml = '';
    curPageSelected = [];
    let imgTrHtml = '<tr>';
    let textTrHtml = '<tr>';
    for (let i = 0; i < data.length; i++) {

        let chk = "";
        for (var j in selIds) {
            if (selIds[j].attachid == data[i].id) {
                chk = "checked";
                curPageSelected.push(data[i].id);
            }
        }

        imgTrHtml += '<td width="20%"><img width="160" height="160" onclick="showBigImg(\'' + data[i].url + '\')" src="' + data[i].thumbnail + '"></td>';
        textTrHtml += '<td width="20%">' +
            '<input type="checkbox" name="imageChbox" id="imageChbox" ' + chk + ' value="' + data[i].id + '"><span>' + data[i].name + '</span></td>';
        if (i == 4) {
            imgTrHtml += '</tr>';
            textTrHtml += '</tr>';
            tableHtml = imgTrHtml + textTrHtml;
            imgTrHtml = '<tr>';
            textTrHtml = '<tr>';
        }
    }

    if (data.length < 4) {
        for (let j = 0; j < (5 - data.length); j++) {
            imgTrHtml += '<td width="20%">&nbsp;</td>';
            textTrHtml += '<td width="20%">&nbsp;</td>';
        }
    }

    imgTrHtml += '</tr>';
    textTrHtml += '</tr>';
    tableHtml += imgTrHtml + textTrHtml;

    if (data.length == 0) {
        tableHtml = '<tr><td>当前条件下无数据</td></tr>'
    }

    $('#relImgMgtTable').html(tableHtml);
}

function showPlanMgtRelImg(data) {
    $('#curDocId').val(data.id);
    if ($('.viewImgMgtBox').is(':hidden')) {
        //alert('--abc--')
        queryViewRelImgMgt(1);
        $('.viewImgMgtBox').show();
    } else {
        return false;
    }
}

function planMgt() {
    if ($('.planMgtBox').is(':hidden')) {
        $('.planMgtBox').show();
        planMgtTable = $('#tabPlanMgtBox').DataTable({
            "language": dataTablesLang,
            "order": [[0, 'desc']],
            "ajax": "/hfxt/planMgt",
            "columns": [
                {'data': "name"},
                {'data': "docDesc"},
                {'data': "fileName"},
                {'data': "fileName"},
            ],

            "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                $('td:eq(3)', nRow).html(
                    "<a class=\"btn btn-link btn-sm wpTableLink pEdit\" role=\"button\">编辑</a>" +
                    "<a class=\"btn btn-link btn-sm wpTableLink pRemove\" role=\"button\">删除</a>" +
                    "<a class=\"btn btn-link btn-sm wpTableLink pShow\" role=\"button\">预览</a>"
                    //+
                    //"<a class=\"btn btn-link btn-sm wpTableLink realImg\" role=\"button\">关联图件</a>" +
                    //"<a class=\"btn btn-link btn-sm wpTableLink showRealImg\" role=\"button\">查看图件</a>"
                );
                return nRow;
            }
        });

        planMgtTable.on('draw', function () {
            $('.wpTableLink').unbind('click').on('click', function (event) {
                event.preventDefault();

                //本行对应的数据
                var data = planMgtTable.row($(this).parents('tr')).data();
                if ($(this).hasClass('pEdit')) {
                    planMgtEdit(data);
                }
                else if ($(this).hasClass('pRemove')) {
                    planMgtRemove(data);
                }
                else if ($(this).hasClass('realImg')) {
                    planMgtRelImg(data);
                }
                else if ($(this).hasClass('showRealImg')) {
                    showPlanMgtRelImg(data);
                }
                else if ($(this).hasClass('pShow')) {
                    showPdfFile(data);
                }
                if ($(this).hasClass('pImages')) {
                    planMgtTable.destroy();
                    $('.waterPersonBox').hide();
                }
            });
        });

    } else {

        return false;

    }

}

function showPdfFile(d) {
    window.open("/pdf/show?fname=" + d.url);
}

function dialogConfirm(title, msg, cnfFun) {

    BootstrapDialog.confirm({
        title: title,
        message: msg,
        type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
        closable: true, // <-- Default value is false
        draggable: true, // <-- Default value is false
        btnCancelLabel: '取消', // <-- Default value is 'Cancel',
        btnOKLabel: '确定', // <-- Default value is 'OK',
        btnOKClass: 'btn-warning', // <-- If you didn't specify it, dialog type will be used,
        callback: function (result) {
            // result will be true if button was click, while it will be false if users close the dialog directly.
            if (result) {
                //alert('Yup.');
                cnfFun(result);
            } else {
                //alert('Nope.');
            }
        }
    });

}

function dialogAlert(title, msg, btn) {
    BootstrapDialog.show({
        title: title,
        message: msg,
        buttons: [{
            label: btn,
            action: function (dialog) {
                dialog.close();
            }
        }]
    });
}