function forecast() {
    $('#dlgForecastMgtFormInputInvalid').text("");
    $('#dlgForecastMgtFormInputInvalid').addClass('hidden');
    //弹出对话框
    $('#forcecastMgt').modal({
        backdrop: 'static'
    });
}


function showAllSite() {
    $.ajax({
        url: '/hfxt/querySitesByType',
        type: 'get',
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            //成功
            if (data.status == 200) {
                //重新加载table数据.
                let d = data.data;
                let points = [];

                for (let i = 0; i < d.length; i++) {
                    points.push({
                        id: d[i].id,
                        name: d[i].name,
                        lng: d[i].longitude,
                        lat: d[i].latitude,
                        type: d[i].type
                    })
                }
                m.markAllSatations(points);
                $('#forcecastMgt').modal('hide');
            } else {
                //显示错误提示信息
                dialogAlert("操作结果", data.message, "确定");
            }
        },
        error: function (err) {
            alert(JSON.stringify(err));
            //location.href = '/';
        }
    });
}


function forecastAction() {
    let baseFollow = 443.3;
    let fFollow = parseFloat($('#forecastFollow').val());
    let fTime = moment.now();
    let baseRate = fFollow / baseFollow;
    //输入不合法
    if (!fFollow) {
        //$('#dlgForecastMgtFormInputInvalid').text("洪峰值不能为空");
        //$('#dlgForecastMgtFormInputInvalid').removeClass('hidden');
        dialogAlert("提示", "洪峰值不能为空", "确定");
        return false;
    }

    $.ajax({
        url: '/hfxt/querySitesByType?type=1',
        type: 'get',
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            //成功
            if (data.status == 200) {
                console.log(JSON.stringify(data));
                //重新加载table数据.
                let d = data.data;
                let points = [];

                for (let i = 0; i < d.length; i++) {
                    points.push({
                        name: d[i].name,
                        lng: d[i].longitude,
                        lat: d[i].latitude,
                        flow: (d[i].twentyh * baseRate).toFixed(2),
                        time: moment(fTime).add(37, "m").format('YYYY-MM-DD HH:mm:ss')
                    });

                }

                //for (let i = 0; i < d.length; i++) {
                //    if (i == 0) {
                //        points.push({
                //            name: d[i].name,
                //            lng: d[i].longitude,
                //            lat: d[i].latitude,
                //            flow: (fFollow * 1.5).toFixed(2),
                //            time: moment(fTime).add(3, "m").format('YYYY-MM-DD HH:mm:ss')
                //        });
                //    }
                //    if (i == 1) {
                //        points.push({
                //            name: d[i].name,
                //            lng: d[i].longitude,
                //            lat: d[i].latitude,
                //            flow: (fFollow * 1.2).toFixed(2),
                //            time: moment(fTime).add(15, "m").format('YYYY-MM-DD HH:mm:ss')
                //        });
                //    }
                //    if (i == 2) {
                //        points.push({
                //            name: d[i].name,
                //            lng: d[i].longitude,
                //            lat: d[i].latitude,
                //            flow: (fFollow * 1.3).toFixed(2),
                //            time: moment(fTime).add(18, "m").format('YYYY-MM-DD HH:mm:ss')
                //        });
                //    }
                //    if (i == 3) {
                //        points.push({
                //            name: d[i].name,
                //            lng: d[i].longitude,
                //            lat: d[i].latitude,
                //            flow: (fFollow * 1.3).toFixed(2),
                //            time: moment(fTime).add(19, "m").format('YYYY-MM-DD HH:mm:ss')
                //        });
                //    }
                //    if (i == 4) {
                //        points.push({
                //            name: d[i].name,
                //            lng: d[i].longitude,
                //            lat: d[i].latitude,
                //            flow: ((d[i].fiveh * 0.99) + 7).toFixed(2),
                //            time: moment(fTime).add(23, "m").format('YYYY-MM-DD HH:mm:ss')
                //        });
                //    }
                //    if (i == 5) {
                //        points.push({
                //            name: d[i].name,
                //            lng: d[i].longitude,
                //            lat: d[i].latitude,
                //            flow: ((d[i].fiveh * 0.99) + 4).toFixed(2),
                //            time: moment(fTime).add(29, "m").format('YYYY-MM-DD HH:mm:ss')
                //        });
                //    }
                //    if (i == 6) {
                //        points.push({
                //            name: d[i].name,
                //            lng: d[i].longitude,
                //            lat: d[i].latitude,
                //            flow: (fFollow * 1.25).toFixed(2),
                //            time: moment(fTime).add(37, "m").format('YYYY-MM-DD HH:mm:ss')
                //        });
                //    }
                //}
                m.showAllSatations(points);
                let forecastHtmls = "<tr> <td>地点</td> <td>水位</td> <td>预计到达时间</td> </tr>";
                for (let k in points) {
                    forecastHtmls += "<tr> <td>" + points[k].name + "</td> <td>" + points[k].flow + "米</td> <td>" + points[k].time + "</td> </tr>"
                }
                $('#forecastTable').html(forecastHtmls);
                $('#forcecastMgt').modal('hide');
            } else {
                //显示错误提示信息
                dialogAlert("操作结果", data.message, "确定");
            }
        },
        error: function (err) {
            alert(JSON.stringify(err));
            //location.href = '/';
        }
    });
}

function showPic(id) {
    alert('hello');
    window.open("http://www.upinvr.com/case/hangpai/46.html");
}

function drawPoints(year) {
    m.clearOverlays();
    if (parseInt(year) > 5) {
        return false;
    }
    $.ajax({
        url: '/hfxt/getPoints/' + year,
        type: 'get',
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            //成功
            if (data.status == 200) {
                //重新加载table数据.
                let d = data.data.points;
                let bgs = data.data.bg;
                for (let j in bgs) {
                    m.markBGSquareLocate(bgs[j]);
                }
                for (let i in d) {
                    m.markSquareLocate(d[i]);
                }
                let p = data.data.paths;

                for (let j in p) {
                    m.markPathLocate(p[j]);
                }
            } else {
                //显示错误提示信息
                dialogAlert("操作结果", data.message, "确定");
            }
        },
        error: function (err) {
            alert(JSON.stringify(err));
            //location.href = '/';
        }
    });

}

function dialogConfirm(title, msg, cnfFun) {

    BootstrapDialog.confirm({
        title: title,
        message: msg,
        type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
        closable: true, // <-- Default value is false
        draggable: true, // <-- Default value is false
        btnCancelLabel: '取消', // <-- Default value is 'Cancel',
        btnOKLabel: '确定', // <-- Default value is 'OK',
        btnOKClass: 'btn-warning', // <-- If you didn't specify it, dialog type will be used,
        callback: function (result) {
            // result will be true if button was click, while it will be false if users close the dialog directly.
            if (result) {
                //alert('Yup.');
                cnfFun(result);
            } else {
                //alert('Nope.');
            }
        }
    });

}

function dialogAlert(title, msg, btn) {
    BootstrapDialog.show({
        title: title,
        message: msg,
        buttons: [{
            label: btn,
            action: function (dialog) {
                dialog.close();
            }
        }]
    });
}