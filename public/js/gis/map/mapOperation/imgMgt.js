/**
 * Created by zhoulanhong on 3/29/17.
 */

var imgAray = [];
var imgName = "";
function setImgSrc(arrarys) {
    imgAray = arrarys;
}

$(document).ready(function () {
    $('#closeImgMgt').click(function () {
        $('.imgMgtBox').hide();
    });


    $('#closeSingleImgMgt').click(function () {
        $('.singleImgMgtBox').hide();
    });


    $('#searchImgMgt').click(function () {
        imgMgt($('#searchImgMgtType').val(), $('#searchWPName').val(), $('#searchImgName').val(), '');
    });

    $('#addImgMgt').click(function () {
        //console.log($("#imgfile[num=0]"));
        //console.log($("#imgfile[num=1]"));
        //console.log($("#imgfile[num=2]"));
        //console.log($("#imgfile[num=3]"));
        //console.log($("#imgfile[num=4]"));
        resetImgBox();
        let selectHtml = '<select id="imgMgtWpName">';
        $.getJSON('/hfxt/waterPerson', function (json) {
            let d = json.data;
            for (var i in d) {
                selectHtml += '<option value="' + d[i].name + '">' + d[i].name + '</option>';
            }
            selectHtml += '</select>';

            $('#imgMgtWaterPerson').html(selectHtml);

            $('#imgMgtSave').text("保存");
            $('#imgMgtSave').prop('disabled', false);

            $('#imgMgtName').val('');

            $('#imgMgtDialog').modal({
                backdrop: 'static'
            });

        });
    });

    $('#imgMgtSave').on('click', function (event) {
        event.preventDefault();

        var imgMgtName = $('#imgMgtName').val();
        var imgMgtType = $('#imgMgtType').val();
        var imgMgtWpName = $('#imgMgtWpName').val();
        var imgMgtWater = $('#waterStateId').val();

        if (imgAray.length == 0) {
            $('#dlgImgMgtFormInputInvalid').removeClass('hidden');
            $('#dlgImgMgtFormInputInvalid').text("请先上传图片!");
        } else if (imgMgtName && imgMgtType && imgMgtWpName && imgAray) {
            $('#imgMgtSave').text("数据保存中...");
            $('#imgMgtSave').prop('disabled', true);
            let url = '/hfxt/imgMgt';
            let dataJson = {
                "imgMgtName": imgMgtName,
                "imgMgtType": imgMgtType,
                "imgMgtWpName": imgMgtWpName,
                "imgMgtWater": imgMgtWater,
                "images": imgAray
            }

            $.ajax({
                url: url,
                dataType: 'json',
                type: 'post',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(dataJson),
                success: function (data) {
                    //成功
                    if (data.status == 200) {
                        $('#imgMgtDialog').modal('hide');
                        //如果是新增marker,则需要直接在地图上添加一个marker
                        queryImgMgt(null, null, null, 1, null);
                    } else {
                        //显示错误提示信息
                        $('#dlgImgMgtFormInputInvalid').text(data.message);
                        $('#dlgImgMgtFormInputInvalid').removeClass('hidden');

                        $('#imgMgtSave').text("保存");
                        $('#imgMgtSave').prop('disabled', false);
                    }
                },
                error: function (err) {
                    location.href = '/';
                }
            });
        } else {
            //输入不合法
            if (!imgMgtName) {
                $('#dlgImgMgtFormInputInvalid').text("图件名称不能为空");
            }
            $('#dlgImgMgtFormInputInvalid').removeClass('hidden');
        }
    });

})

function showSigleImgMgt(type, wpName, nameKey, waterName) {
    $('.singleImgMgtBox').show();
    querySingleImgMgt(type, wpName, nameKey, 1, waterName)
}

function querySingleImgMgt(type, wpName, nameKey, page, waterName) {

    $.getJSON('/hfxt/singleImgMgt', {
        page: page,
        wpName: wpName,
        imgType: type,
        name: nameKey,
        waterState: waterName
    }, function (json) {
        createSingleTable(json.data.rows);
        if (json.data.cnt > 0) {
            $('.sinImgM-box').pagination({
                totalData: json.data.cnt,
                showData: 1,
                current: page,
                callback: function (api) {
                    querySingleImgMgt(type, wpName, nameKey, api.getCurrent(), waterName);
                }
            });
        } else {
            $('.sinImgM-box').html('');
        }
    });
}


function imgMgt(type, wpName, nameKey, waterName) {

    if (wpName) {
        $('#searchWPName').val(wpName);
    }

    if ($('.imgMgtBox').is(':hidden')) {
        $('.imgMgtBox').show();
        queryImgMgt(type, wpName, nameKey, 1, waterName);

    } else {

        queryImgMgt(type, wpName, nameKey, 1, waterName);

    }
}

function queryImgMgt(type, wpName, nameKey, page, waterName) {
    $.getJSON('/hfxt/imgMgt', {
        page: page,
        wpName: wpName,
        imgType: type,
        name: nameKey,
        waterState: waterName
    }, function (json) {
        createTable(json.data.rows);
        if (json.data.cnt > 0) {
            $('.imgM-box').pagination({
                totalData: json.data.cnt,
                showData: 10,
                current: page,
                callback: function (api) {
                    queryImgMgt(type, wpName, nameKey, api.getCurrent(), waterName);
                }
            });
        } else {
            $('.imgM-box').html('');
        }
    });
}

function showBigImg(url) {
    window.open(url);
}

function deleteImg(id) {

    dialogConfirm("请确认", "确定要删除本条数据？", function (result) {
        if (result) {
            $.ajax({
                url: '/hfxt/delImgMgt',
                dataType: 'json',
                type: 'post',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({
                    id: id
                }),
                success: function (data) {
                    //成功
                    if (data.status == 200) {
                        //重新加载table数据.
                        imgMgt(null, null);
                    } else {
                        //显示错误提示信息
                        dialogAlert("操作结果", data.message, "确定");
                    }
                },
                error: function (err) {
                    alert(JSON.stringify(err));
                    //location.href = '/';
                }
            });
        }
    });

}

function createSingleTable(data) {

    let tableHtml = '';
    let imgTrHtml = '<tr>';
    let textTrHtml = '<tr>';
    for (let i = 0; i < data.length; i++) {
        imgTrHtml += '<td width="100%" align="center"><img width="420" height="420" src="' + data[i].url + '"></td>';
        textTrHtml += '<td width="100%"  align="center">' + data[i].name + '</td>';
    }

    imgTrHtml += '</tr>';
    textTrHtml += '</tr>';
    tableHtml += imgTrHtml + textTrHtml;

    if (data.length == 0) {
        tableHtml = '<tr><td>当前条件下无数据</td></tr>'
    }

    $('#singleImgMgtTable').html(tableHtml);
}

function mgtTypeChange() {
    if ($('#imgMgtType').val() == "2") {
        $('#mgtTypeTr').show();
    } else {
        $('#mgtTypeTr').hide();
    }
}

function createTable(data) {

    let tableHtml = '';
    let imgTrHtml = '<tr>';
    let textTrHtml = '<tr>';
    for (let i = 0; i < data.length; i++) {
        imgTrHtml += '<td width="20%"><img width="160" height="160" onclick="showBigImg(\'' + data[i].url + '\')" src="' + data[i].thumbnail + '"></td>';
        textTrHtml += '<td width="20%" onclick="deleteImg(\'' + data[i].id + '\')">' + data[i].name + '</td>';
        if (i == 4) {
            imgTrHtml += '</tr>';
            textTrHtml += '</tr>';
            tableHtml = imgTrHtml + textTrHtml;
            imgTrHtml = '<tr>';
            textTrHtml = '<tr>';
        }
    }

    if (data.length < 4) {
        for (let j = 0; j < (5 - data.length); j++) {
            imgTrHtml += '<td width="20%">&nbsp;</td>';
            textTrHtml += '<td width="20%">&nbsp;</td>';
        }
    }

    imgTrHtml += '</tr>';
    textTrHtml += '</tr>';
    tableHtml += imgTrHtml + textTrHtml;

    if (data.length == 0) {
        tableHtml = '<tr><td>当前条件下无数据</td></tr>'
    }

    $('#imgMgtTable').html(tableHtml);
}

function dialogConfirm(title, msg, cnfFun) {

    BootstrapDialog.confirm({
        title: title,
        message: msg,
        type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
        closable: true, // <-- Default value is false
        draggable: true, // <-- Default value is false
        btnCancelLabel: '取消', // <-- Default value is 'Cancel',
        btnOKLabel: '确定', // <-- Default value is 'OK',
        btnOKClass: 'btn-warning', // <-- If you didn't specify it, dialog type will be used,
        callback: function (result) {
            // result will be true if button was click, while it will be false if users close the dialog directly.
            if (result) {
                cnfFun(result);
            } else {
            }
        }
    });

}

function dialogAlert(title, msg, btn) {
    BootstrapDialog.show({
        title: title,
        message: msg,
        buttons: [{
            label: btn,
            action: function (dialog) {
                dialog.close();
            }
        }]
    });
}