/**
 * Created by zhoulanhong on 3/16/17.
 */
/**
 * Created by zhoulanhong on 1/6/17.
 */
var topMargin;
var leftMargin;
//当前正在画的矩形对象
var chooseType;
var Rectagle = {

    obj: null,//画布
    container: null,//初始化函数
    init: function (containerId,type) {
        chooseType = type;
        Rectagle.container = document.getElementById(containerId);
        if (Rectagle.container) {
            Rectagle.container.onmousedown = Rectagle.start;//鼠标按下时开始画
            Rectagle.container.onmouseout = Rectagle.destory;
        } else {
            alert('请指定正确的容器!');
        }
    },
    destory: function (e) {
        Rectagle.container.onmousedown = null;
        Rectagle.obj = null;

        document.getElementById("divMouseSelect").style.display = "none";
    },
    start: function (e) {
        Rectagle.container.onmouseout = null;

        var o = Rectagle.obj = document.createElement('div');
        o.style.position = "absolute";
        // mouseBeginX，mouseBeginY是辅助变量，记录下鼠标按下时的位置
        o.mouseBeginX = Rectagle.getEvent(e).x - leftMargin;
        o.mouseBeginY = Rectagle.getEvent(e).y - topMargin;
        o.style.left = o.mouseBeginX + "px";
        o.style.top = o.mouseBeginY + "px";
        o.style.height = 0;
        o.style.width = 0;
        o.style.border = "dotted black 1px";
        o.style.backgroundColor = "#FFFF00";

        //把当前画出的对象加入到画布中
        Rectagle.container.appendChild(o);
        //处理onmousemove事件
        Rectagle.container.onmousemove = Rectagle.move;
        //处理onmouseup事件
        Rectagle.container.onmouseup = Rectagle.end;
    },
    move: function (e) {
        var o = Rectagle.obj;
        //dx，dy是鼠标移动的距离
        var dx = Rectagle.getEvent(e).x - o.mouseBeginX - leftMargin;
        var dy = Rectagle.getEvent(e).y - o.mouseBeginY - topMargin;
        //如果dx，dy <0,说明鼠标朝左上角移动，需要做特别的处理
        if (dx < 0) {
            o.style.left = (Rectagle.getEvent(e).x - leftMargin) + "px";
        }
        if (dy < 0) {
            o.style.top = (Rectagle.getEvent(e).y - topMargin) + "px";
        }
        o.style.height = Math.abs(dy) + "px";
        o.style.width = Math.abs(dx) + "px";
    },
    removeAllChild: function (obj) {
        while (obj.lastChild) {
            obj.removeChild(obj.lastChild);
        }
    },
    end: function (e) {
        // alert('--end--');
        //通过div的位置和长宽,计算出选择的区域
        Rectagle.chooseResult();
        //去掉所有新创建的 div
        Rectagle.removeAllChild(Rectagle.container);
        //onmouseup时释放onmousemove，onmouseup事件句柄
        Rectagle.container.onmousedown = null;
        Rectagle.container.onmousemove = null;
        Rectagle.container.onmouseup = null;
        Rectagle.obj = null;

        //document.getElementById("divMouseSelect").style.display = "none";
        $('#divMouseSelect').hide();
        //if (document.getElementById("multipleChooseEmps").value.length > 0) {
        //    //www.openWin(270, 70, 500, 350, "./page/mis-create.jsp");
        //    alert('end');
        //}
    },
    //辅助方法，处理IE和FF不同的事件模型
    getEvent: function (e) {
        if (e == undefined) {
            e = window.event;
        }
        if (e.x == undefined) {
            e.x = e.pageX;
        }
        if (e.y == undefined) {
            e.y = e.pageY;
        }
        return e;
    },
    chooseResult: function () {
        var startX = Rectagle.obj.offsetLeft;
        var startY = Rectagle.obj.offsetTop;
        var endX = Rectagle.obj.offsetWidth + startX;
        var endY = Rectagle.obj.offsetHeight + startY;
        //alert(startX + " " + startY + "  " + endX + "  " + endY);
        //alert(map.pixelToPoint(startX,startY));
        var startPoint = m.pixelToPoint(startX, startY);
        var endPoint = m.pixelToPoint(endX, endY);
        chooseBySelect(startPoint, endPoint,chooseType);
        //document.getElementById("multipleChooseEmps").value = groupSelected(startPoint, endPoint);
    }
};

function mouseSelect(type) {
    //    $('#divMouseSelect').show();
    showdivMouseSelect();
    Rectagle.init("divMouseSelect",type);
}

function showdivMouseSelect() {
    //复制整个 allmap 的位置属性,用于完全 cover 做 allmap
    var dwidth = document.getElementById("allmap").offsetWidth;
    var dheight = document.getElementById("allmap").offsetHeight;
    var dtop = document.getElementById("allmap").offsetTop;
    var dleft = document.getElementById("allmap").offsetLeft;
    document.getElementById("divMouseSelect").style.width = dwidth + "px";
    document.getElementById("divMouseSelect").style.height = dheight + "px";
    document.getElementById("divMouseSelect").style.top = dtop + "px";
    document.getElementById("divMouseSelect").style.left = dleft + "px";

    topMargin = dtop;
    leftMargin = dleft;
    $('#divMouseSelect').show();
}

//根据界面上框选的范围,来对组织树进行选择
function chooseBySelect(startPoint, endPoint,type) {
    var treeObj = $.fn.zTree.getZTreeObj("orgtree");
    var allNodes = treeObj.transformToArray(treeObj.getNodes());
    treeObj.checkAllNodes(false);
    for (var i in allNodes) {
        /* 四个条件:
         * 1.有最后一条坐标记录
         * 2.当前在线
         * 3.在组织树中没有被选中
         * 4.经纬度在坐标之内
         * */

        if (allNodes[i].children && allNodes[i].children.length > 0) {
            // alert(treeObj.expandNode(allNodes[i], true, true, true));
            continue;
        }
        // console.log(i + "-i-" + JSON.stringify(allNodes[i]));
        //  onlineMap.get(allNodes[i].idNumber + "") &&   //如果需要只选择在线的,那么就要加上这个条件
        // console.log(allNodes[i].getParentNode());
        if (allNodes[i].lastLocation && !allNodes[i].checked &&
            (allNodes[i].lastLocation.coordinates[0] > startPoint.lng &&
            allNodes[i].lastLocation.coordinates[0] < endPoint.lng &&
            allNodes[i].lastLocation.coordinates[1] < startPoint.lat &&
            allNodes[i].lastLocation.coordinates[1] > endPoint.lat)) {
            var pnode = allNodes[i].getParentNode();
            if (pnode) {
                treeObj.expandNode(pnode, true, true, true);
                $('#' + ($('#hideStaffElement_' + allNodes[i].idNumber).val())  + '_check').click();
               var checkedNodes = pnode
                intercomCall(type);
                for (var i in checkedNodes.children) {
                    if (!checkedNodes.children[i].isParent && checkedNodes.children[i].lastLocation &&
                        checkedNodes.children[i].lastLocation.coordinates[0] > startPoint.lng &&
                        checkedNodes.children[i].lastLocation.coordinates[0] < endPoint.lng &&
                        checkedNodes.children[i].lastLocation.coordinates[1] < startPoint.lat &&
                        checkedNodes.children[i].lastLocation.coordinates[1] > endPoint.lat) {
                         treeObj.checkNode(checkedNodes.children[i],true,true,false);
                        m.locate({
                            id: checkedNodes.children[i]._id,
                            lat: checkedNodes.children[i].lastLocation.coordinates[0],
                            lng: checkedNodes.children[i].lastLocation.coordinates[1],
                            speed: "0.0KM/H",
                            iconPath: checkedNodes.children[i].locateicon,
                            name: checkedNodes.children[i].name,
                            userid: checkedNodes.children[i].idNumber,
                            lastLocationTime: checkedNodes.children[i].lastLocationTimeCST,
                            position: checkedNodes.children[i].positionname
                        }, 1);

                    }
                }
            }
        }
    }
}