/**
 * Created by zhoulanhong on 3/16/17.
 */
var imgAray = [];
var imgName  = "";
function getImgSrc(arrarys) {
    imgAray = arrarys;
}
/**
 点击地图操作的搜索按钮，显示地图搜索框
 */
var Marker = new function () {

    this.markerFlg = false;
    this.curMarker = null;
    var that = this;

    this.getMarkerFlg = function () {
        return this.markerFlg;
    }

    this.showMarkerView = function () {
        //弹出对话框
        $('#mapSigoViewdialog').modal({
            backdrop: 'static'
        });

        that.markerFlg = false;

    }
    $('#mapSigoView').on('click', function (event) {
        event.preventDefault();
        that.markerFlg = true;
        $('#dlgMapSigoFormInputInvalid').text("");
        $('#dlgMapSigoFormInputInvalid').addClass('hidden');
        $('#sigoName').val("");
        $('#sigoContent').val("");
        $('#markSaveMode').val("");
        $('.lookimg').remove();

        $('#btnMapSigoSave').text("保存");
        $('#btnMapSigoSave').prop('disabled', false);

        $('#markType').html('<img id="sigoType" src="/images/sigo/ui-28.gif" ' + '>');
    });


    $('#btnMapSigoSave').on('click', function (event) {
        event.preventDefault();

        var imgpath = $('#sigoType').attr("src");
        var name = $('#sigoName').val();
        var desc = $('#sigoContent').val();
        var typeicon = imgpath;

        var loacl = $('#markLocation').val();
        var location = loacl.split(",");
        var imgLength=$('.lookimg').length;
        if(imgAray.length==0 && imgLength!=0){
            $('#dlgMapSigoFormInputInvalid').removeClass('hidden');
            $('#dlgMapSigoFormInputInvalid').text("请先上传图片!");
        }else if (name && desc && typeicon && imgAray) {
            $('#btnMapSigoSave').text("数据保存中...");
            $('#btnMapSigoSave').prop('disabled', true);
            var url;
            var dataJson;
            if ($('#markSaveMode').val().length == 0) {
                url = '/api/marker';
                dataJson = {
                    "name": name,
                    "description": desc,
                    "type": typeicon,
                    "location": {
                        "lng": location[0],
                        "lat": location[1]
                    },
                    "images": imgAray
                }
            } else {
                url = '/api/updMarker';
                dataJson = {
                    "_id": $('#markSaveMode').val(),
                    "name": name,
                    "description": desc,
                    "type": typeicon,
                    "location": {
                        "lng": location[0],
                        "lat": location[1]
                    },
                    "images": imgAray
                }
            }
            $.ajax({
                url: url,
                dataType: 'json',
                type: 'post',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(dataJson),
                success: function (data) {
                    //成功
                    if (data.status == 200) {
                        $('#mapSigoViewdialog').modal('hide');
                        //如果是新增marker,则需要直接在地图上添加一个marker
                        if($('#markSaveMode').val().length == 0){
                            m.markLocate({
                                _id: data.marker._id,
                                lag: data.marker.location.lat,
                                lng: data.marker.location.lng,
                                iconPath: data.marker.type,
                                name: data.marker.name,
                                descriptions: data.marker.description,
                                markimages: data.marker.images
                            });
                        }

                    } else {
                        //显示错误提示信息
                        $('#dlgMapSigoFormInputInvalid').text(data.message);
                        $('#dlgMapSigoFormInputInvalid').removeClass('hidden');

                        $('#btnMapSigoSave').text("保存");
                        $('#btnMapSigoSave').prop('disabled', false);
                    }
                },
                error: function (err) {
                    location.href = '/';
                }
            });
        } else {
            //输入不合法
            if (!name) {
                $('#dlgMapSigoFormInputInvalid').text("标记名称不能为空");
            }
            else if (!typeicon) {
                $('#dlgMapSigoFormInputInvalid').text("标记类型不能为空");
            }
            else if (!desc) {
                $('#dlgMapSigoFormInputInvalid').text("标记详情不能为空");
            }
            //else if (!markImg) {
            //    $('#dlgMapSigoFormInputInvalid').text("标记图片不能为空");
            //}
            $('#dlgMapSigoFormInputInvalid').removeClass('hidden');
        }
    });

    $('#btnMarkerDelete').on('click', function (event) {
        dialogConfirm("请确认", "确定要删除本条标记吗？", function (result) {
            if (result) {
                $.ajax({
                    url: '/api/delMarker',
                    dataType: 'json',
                    type: 'post',
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify({
                        _id: $('#markSaveMode').val()
                    }),
                    success: function (data) {
                        //成功
                        if (data.status == 200) {
                            //清除当前点
                            dialogAlert("操作结果", "删除标记成功", "确定");
                            $('#mapSigoViewdialog').modal('hide');
                            m.clearOverlays(that.curMarker);
                        } else {
                            //显示错误提示信息
                            dialogAlert("操作结果", data.message, "确定");
                        }
                    },
                    error: function (err) {
                        location.href = '/';
                    }
                });
            }
        });
    });


    this.editMark = function (data) {
        //显示删除按钮
        $('#btnMarkerDelete').show();

        //弹出对话框
        $('#mapSigoViewdialog').modal({
            backdrop: 'static'
        });

        $('#dlgMapSigoFormInputInvalid').addClass('hidden');
        $('#markSaveMode').val(data._id);
        $('#sigoName').val(data.name);
        $('#sigoContent').val(data.desc);
        that.curMarker = data.curMarker;
        imgName = data.markimages;
        $('#btnMapSigoSave').text("保存");
        $('#btnMapSigoSave').prop('disabled', false);
        $('.lookimg').remove();
        $('#markType').html('<img id="sigoType" src=' + data.iconPath + ' ' + '>');
        var path2 = data.markimages;
        if (path2 && $('.lookimg').length == 0) {
            for (var i in path2) {
                //创建预览外层
                var _prevdiv = document.createElement("div");
                _prevdiv.setAttribute("class", "lookimg");
                //创建内层img对象
                var preview = document.createElement("img");
                preview.setAttribute("src", path2[i]);
                $(_prevdiv).append(preview);
                //创建放大按钮
                var IMG_MOREBIG = document.createElement("div");
                IMG_MOREBIG.setAttribute("class", "lookimg_moreBig");
                IMG_MOREBIG.setAttribute("id", "moreBig");
                $(_prevdiv).append(IMG_MOREBIG);
                //创建删除按钮
                var IMG_DELBTN = document.createElement("div");
                IMG_DELBTN.setAttribute("class", "lookimg_delBtn");
                IMG_DELBTN.innerHTML = "移除";
                $(_prevdiv).append(IMG_DELBTN);

                //在预览图中创建li容器
                var IMG_SHOWMOREBIG = document.createElement("li");
                $('#imgUl').append(IMG_SHOWMOREBIG);
                //在li里面创建对应的img图片
                var showBigImg = document.createElement("img");
                showBigImg.setAttribute("src", path2[i]);
                $(IMG_SHOWMOREBIG).append(showBigImg);
                //在图片下面创建对应的小圆点
                var showImgIcon = document.createElement("span");
                showImgIcon.setAttribute("class", "i");
                $('#iconUl').append(showImgIcon);
                //创建进度条
                var IMG_PROGRESS = document.createElement("div");
                IMG_PROGRESS.setAttribute("class", "lookimg_progress");
                $(IMG_PROGRESS).append(document.createElement("div"));
                $(_prevdiv).append(IMG_PROGRESS);
                //记录此对象对应编号
                _prevdiv.setAttribute("num", i);
                //对象注入界面
                $("#div_imglook").children("div:last").before(_prevdiv);
                UP_IMGCOUNT++;//编号增长防重复

            }
        }

        that.markerFlg = true;

    }
    this.getMarkInfor = function () {
        $.ajax({

            url: '/api/marker',
            //url: '/api/updMarker',
            dataType: 'json',
            type: 'get',
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                that.showicon(data.data);
            },
            error: function (err) {
            }
        });
    }

    this.showicon = function (data) {
        for (var i in data) {
            m.markLocate({
                _id: data[i]._id,
                lag: data[i].location.lat,
                lng: data[i].location.lng,
                iconPath: data[i].type,
                name: data[i].name,
                descriptions: data[i].description,
                markimages: data[i].images
            });
        }
        //console.log("++++++++++0000++++++++++++++++++++"+iconPathl.length);
    }
    this.addmarkIcon = function () {
        return $('#imgData').click();
    }
}();