/**
 * Created by zhoulanhong on 3/29/17.
 */

let curSelectedIds = [];
let persons = null;
let needSendMsgIds = [];

$(document).ready(function () {
    $('#closeSendSMS').click(function () {
        $('.semdSMSBox').hide();
    });

    $('#btnWaterPersonSave').click(function () {

        if ($('#msgtext').val().length == 0) {
            dialogAlert('提示消息', '请输入需要发送的消息', '确定');
            return;
        }

        if ($('#msgtime').val().length == 0) {
            dialogAlert('提示消息', '请选择消息时间', '确定');
            return;
        }

        if ($('#msgaddress').val().length == 0) {
            dialogAlert('提示消息', '请输入相关地址', '确定');
            return;
        }

        let selPersons = [];
        let selPhoneNames = [];
        $('input[name="selPerson"]:checked').each(function () {
            selPersons.push((($(this).val()).split(","))[1]);
            selPhoneNames.push((($(this).val()).split(","))[2]);
        });

        if (selPersons.length == 0) {
            dialogAlert('提示消息', '请选择需要接收消息的人员', '确定');
            return;
        }

        let dataJson = {
            phoneNbrs: selPersons,
            phoneNames: selPhoneNames,
            msgtext: $('#msgtext').val(),
            msgtime: $('#msgtime').val(),
            msgaddress: $('#msgaddress').val(),
            msgtype: $('#msgType').val()
        };
        //alert(JSON.stringify(dataJson));
        //return;
        $.ajax({
            url: '/hfxt/sendSmsNew/',
            dataType: 'json',
            type: 'post',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(dataJson),
            success: function (data) {
                //成功
                if (data.status == 200) {
                    dialogAlert('提示消息', '消息发送成功', '确定');
                    $('.semdSMSBox').hide();
                } else {
                    //显示错误提示信息
                    //$('#dlgWaterPersonFormInputInvalid').text(data.message);
                    //$('#dlgWaterPersonFormInputInvalid').removeClass('hidden');
                    //
                    //$('#btnWaterPersonSave').text("保存");
                    //$('#btnWaterPersonSave').prop('disabled', false);
                }
            },
            error: function (err) {
                location.href = '/';
            }
        });

    });

});

function saveasGroup() {

    $('input[name="selPerson"]:checked').each(function () {
        curSelectedIds.push(($(this).val()).split(",")[0]);
    });

    if (curSelectedIds.length == 0) {
        dialogAlert('提示消息', '请选择需要保存到组的人员', '确定');
        return;
    }

    $('#wpGroupName').val('');
    $('#smsGroupAdd').show();
}

function cancelGPBox() {
    $('#smsGroupAdd').hide();
}

function btnPersonGroupSave() {
    $('#dlgGroupPersonFormInputInvalid').addClass('hidden');
    //alert("btnPersonGroupSave");

    if ($('#wpGroupName').val().length == 0) {
        //alert("abc");
        $('#dlgGroupPersonFormInputInvalid').text("请输入组名");
        $('#dlgGroupPersonFormInputInvalid').removeClass('hidden');
        return;
    }

    let dataJson = {gpName: $('#wpGroupName').val(), wpIds: curSelectedIds};

    $.ajax({
        url: '/hfxt/group/',
        dataType: 'json',
        type: 'post',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(dataJson),
        success: function (data) {
            //成功
            if (data.status == 200) {
                dialogAlert('提示消息', '保存组成功', '确定');
                generateGp();
                $('#smsGroupAdd').hide();
            } else {
                //显示错误提示信息
            }
        },
        error: function (err) {
            location.href = '/';
        }
    })
}

function deleteGroup() {
    let delGroups = [];
    $('input[name="selGroup"]:checked').each(function () {
        delGroups.push(($(this).val()).split("&")[0]);
    });

    let dataJson = {id: delGroups}

    $.ajax({
        url: '/hfxt/groupDel/',
        dataType: 'json',
        type: 'post',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(dataJson),
        success: function (data) {
            //成功
            if (data.status == 200) {
                dialogAlert('提示消息', '删除组成功', '确定');
                generateGp();
                $('.smsGroupAdd').hide();
            } else {
                //显示错误提示信息
            }
        },
        error: function (err) {
            location.href = '/';
        }
    })

}

function generateGroupSelText(obj) {

    let curGroupSelectIds = ((obj).val().split("&")[1]).split(",");

    $('input[name="selPerson"]').each(function (cur) {
        for (let i in curGroupSelectIds) {
            if (curGroupSelectIds[i] == (($(this).val()).split(",")[0])) {
                if (obj.is(':checked')) {
                    $(this).prop("checked", true);
                } else {
                    $(this).attr("checked", false);
                }
            }
        }
    });

    generateSelText();
}

function sendSms() {
    curSelectedIds = [];
    $.getJSON('/hfxt/waterPerson', function (json) {

        persons = json.data;
        let htmls = '';
        for (let i in persons) {
            htmls += '<tr><td style="padding: 0px 0px 0px 0px;"><input style="margin-top: 0px; width: 20px; height: 20px" type="checkbox" name="selPerson" onclick="generateSelText()" value="' + persons[i].id + ',' + persons[i].phoneNbr + ',' + persons[i].name + '"> <div style="margin-top: 5px">' + persons[i].name + '</div></td> </tr>'
        }

        $('#contactNames').html(htmls);

        if ($('.semdSMSBox').is(':hidden')) {

            $("#msgtime").val("");
            $("#msgaddress").val("");
            $("#msgtext").val("");

            $('.semdSMSBox').show();
        }

    });
    generateGp();
}

function generateGp() {
    $.getJSON('/hfxt/group', function (json) {

        let persons = json.data;
        let gphtmls = '';
        for (let i in persons) {
            gphtmls += '<tr><td style="padding: 0px 0px 0px 0px;"><input style="margin-top: 0px; width: 20px; height: 20px" type="checkbox" name="selGroup" value="' + persons[i].id + '&' + persons[i].wpids + '" onclick="generateGroupSelText($(this))" > <div style="margin-top: 5px">' + persons[i].groupname + '</div></td> </tr>'
        }

        $('#gropuNames').html(gphtmls);

        if ($('.semdSMSBox').is(':hidden')) {
            $('.semdSMSBox').show();
        }

    });
}

function generateSelText() {
    let showPerson = '已选择人员: '
    $('input[name="selPerson"]:checked').each(function () {
        showPerson += ' ' + (($(this).val()).split(","))[2];
    });
    $('#selectedNames').text(showPerson);
}

function chgMsgType() {

    if ($('#msgType').val() == "1") {
        $("#msgtime").attr('placeholder', "请选择会议时间");
        $("#msgaddress").attr('placeholder', "请输入会议地点");
        $("#msgtext").attr('placeholder', "请输入会议通知内容");
    }
    if ($('#msgType').val() == "2") {
        $("#msgtime").attr('placeholder', "请选择水情发生时间");
        $("#msgaddress").attr('placeholder', "请输入水情发生地点");
        $("#msgtext").attr('placeholder', "请输入具体的水情内容");
    }
}

function dialogConfirm(title, msg, cnfFun) {

    BootstrapDialog.confirm({
        title: title,
        message: msg,
        type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
        closable: true, // <-- Default value is false
        draggable: true, // <-- Default value is false
        btnCancelLabel: '取消', // <-- Default value is 'Cancel',
        btnOKLabel: '确定', // <-- Default value is 'OK',
        btnOKClass: 'btn-warning', // <-- If you didn't specify it, dialog type will be used,
        callback: function (result) {
            // result will be true if button was click, while it will be false if users close the dialog directly.
            if (result) {
                //alert('Yup.');
                cnfFun(result);
            } else {
                //alert('Nope.');
            }
        }
    });

}

function dialogAlert(title, msg, btn) {
    BootstrapDialog.show({
        title: title,
        message: msg,
        buttons: [{
            label: btn,
            action: function (dialog) {
                dialog.close();
            }
        }]
    });
}