/**
 * Created by zhoulanhong on 12/29/16.
 */

/**
 * 通过输入在ID,在地图界面上回显用户当前的数据
 * @param treeId 要获取数据的树的ID
 * @param id 要回显的人员id
 * */
function gisSettingUser(treeId, id) {
    //alert(treeId + "  " + id);
    //alert(zTreeObj);
    $('#gisSettingUserIdNumber').val(id);
    var treeObj = $.fn.zTree.getZTreeObj(treeId);
    alert(treeObj);
    //var allNodes = treeObj.transformToArray(treeObj.getNodes());
    var allNodes = treeObj.transformToArray(treeObj.getNodes());
    //alert(allNodes);
    var treeNode = null;
    for (var i in allNodes) {
        if (allNodes[i].idNumber == id) {
            treeNode = allNodes[i];
            //alert('find');
            break;
        }
    }
    //如果没有定位有空,不进行反显
    if (treeNode.locationEnable) {
        $('#locationEnable').click();
    }

    //回显周几天周几
    if (treeNode.locationWeekly) {
        var weeklyCheckbox = ["setMon", "setQue", "setWen", "setThu", "setFri", "setSat", "setSun"];
        var idx = 0;
        treeNode.locationWeekly.forEach(function (it) {
            if (it) {
                $('#' + weeklyCheckbox[idx]).click();
            }
            idx++;
        });
    }

    //回显开始和持继时间
    if (treeNode.locationTime) {
        $('#setStartTime').val(treeNode.locationTime.from);
        $('#lastTime').val(treeNode.locationTime.last);
    }

    //回显定位频率和上传频率
    if (treeNode.locationReportInterval && treeNode.locationScanInterval) {
        if (treeNode.locationReportInterval == treeNode.locationScanInterval) {
            $("input[name='locateUpd']:eq(3)").click();
        } else {
            $("input[type=radio][name='locateFeq'][value=" + treeNode.locationScanInterval + "]").click();
            $("input[type=radio][name='locateUpd'][value=" + treeNode.locationReportInterval + "]").click();
        }

        if (treeNode.locationScanInterval == 10 ||
            treeNode.locationScanInterval == 30 ||
            treeNode.locationScanInterval == 60 ||
            treeNode.locationScanInterval == 180 ||
            treeNode.locationScanInterval == 300) {
            $("input[type=radio][name='locateFeq'][value=" + treeNode.locationScanInterval + "]").click();
        } else {
            $("input[name='locateFeq']:eq(5)").click();
            $('#setLocateFeq').val(treeNode.locationScanInterval / 60);
        }
    }

    var privilegeBoxs = [
        treeNode.privileges.callAble,
        treeNode.privileges.groupAble,
        treeNode.privileges.calledAble,
        treeNode.privileges.joinAble,
        treeNode.privileges.forbidSpeak,
        treeNode.privileges.callOuterAble,
        treeNode.privileges.calledOuterAble,
        treeNode.privileges.powerInviteAble,
        treeNode.privileges.muteAble,
        treeNode.privileges.viewMap,
    ]

    var privilegeLabels = [
        'privCall',
        'privGroup',
        'privRecvCall',
        'privRecvGroup',
        'forbidSpeak',
        'callOuter',
        'recvCallOuter',
        'powerInviteAble',
        'muteAble',
        'viewMap'
    ]

    for (var i in privilegeBoxs) {
        if (privilegeBoxs[i]) {
            $('#' + privilegeLabels[i]).click();
        }
    }
    $("#setStartTime").datetimepicker({
        startView: 1,
        minuteStep: 5,
        format: 'hh:ii',
        autoclose: true,
        language: 'zh-CN'
    });
}

function gisSaveLoc() {
    $('#locationErrMsg').addClass('hidden');
    var locateFeq = $('input:radio[name="locateFeq"]:checked').val();
    var locationEnable = true;
    if (!locateFeq) {
        locationEnable = false;
    }
    if (locateFeq == 0) {
        var setLocateFeq = $('#setLocateFeq').val();
        if (!setLocateFeq) {
            $('#locationErrMsg').text('自定义定位频率,请输入时间');
            $('#locationErrMsg').removeClass('hidden');
        }
        locateFeq = setLocateFeq * 60;
    }

    var locateUpd = $('input:radio[name="locateUpd"]:checked').val();
    if (!locateUpd) {
        locationEnable = false;
    }
    if (locateUpd == 0) {
        locateUpd = locateFeq;
    }

    var selStaffs = [];
    locSelectedStaffs.forEach(function (staff) {
        selStaffs.push(staff.idNumber);
    });
    var weekly = [
        $('#setMon').is(':checked') ? 1 : 0,
        $('#setQue').is(':checked') ? 1 : 0,
        $('#setWen').is(':checked') ? 1 : 0,
        $('#setThu').is(':checked') ? 1 : 0,
        $('#setFri').is(':checked') ? 1 : 0,
        $('#setSat').is(':checked') ? 1 : 0,
        $('#setSun').is(':checked') ? 1 : 0
    ];
    if (!(weekly[0] || weekly[1] || weekly[2] || weekly[3] || weekly[4] || weekly[5] || weekly[6])) {
        locationEnable = false;
    }

    var st = $('#setStartTime').val();
    var et = $('#lastTime').val();
    if (!(st && et)) {
        locationEnable = false;
    }

    if (!locSelectedStaffs || locSelectedStaffs.size == 0) {
        $('#locationErrMsg').text('请选择人员');
        $('#locationErrMsg').removeClass('hidden');
        return;
    }

    if ($('#lastTime').val() < 1 || $('#lastTime').val() > 24) {
        $('#locationErrMsg').text('持续时间必须在1到24小时之间');
        $('#locationErrMsg').removeClass('hidden');
        return;
    }

    if ($('#locationEnable').is(':checked') && locationEnable) {
        locationEnable = true;
    } else {
        locationEnable = false;
    }

    alert(JSON.stringify({
        idNumber: $('#gisSettingUserIdNumber').val(),
        locationWeekly: weekly,
        locationTime: {
            from: $('#setStartTime').val(),
            last: $('#lastTime').val()
        },
        locationScanInterval: locateFeq,
        locationReportInterval: locateUpd,
        locationEnable: locationEnable,
        privileges: {
            callAble: $('#privCall').prop('checked'),
            groupAble: $('#privGroup').prop('checked'),
            calledAble: $('#privRecvCall').prop('checked'),
            joinAble: $('#privRecvGroup').prop('checked'),
            forbidSpeak: $('#forbidSpeak').prop('checked'),
            callOuterAble: $('#callOuter').prop('checked'),
            calledOuterAble: $('#recvCallOuter').prop('checked'),
            powerInviteAble: $('#powerInviteAble').prop('checked'),
            muteAble: $('#muteAble').prop('checked'),
            viewMap: $('#viewMap').prop('checked'),
            priority: priority,
        }
    }));

    return;

    $('#saveLoc').prop('disabled', true);
    $.ajax({
        url: '/api/contact/gisUpdUserLocate',
        dataType: 'json',
        type: 'post',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({
            idNumber: $('#gisSettingUserIdNumber').val(),
            locationWeekly: weekly,
            locationTime: {
                from: $('#setStartTime').val(),
                last: $('#lastTime').val()
            },
            locationScanInterval: locateFeq,
            locationReportInterval: locateUpd,
            locationEnable: locationEnable,
            privileges: {
                callAble: $('#privCall').prop('checked'),
                groupAble: $('#privGroup').prop('checked'),
                calledAble: $('#privRecvCall').prop('checked'),
                joinAble: $('#privRecvGroup').prop('checked'),
                forbidSpeak: $('#forbidSpeak').prop('checked'),
                callOuterAble: $('#callOuter').prop('checked'),
                calledOuterAble: $('#recvCallOuter').prop('checked'),
                powerInviteAble: $('#powerInviteAble').prop('checked'),
                muteAble: $('#muteAble').prop('checked'),
                viewMap: $('#viewMap').prop('checked'),
                priority: priority,
            }
        }),
        success: function (data) {
            //成功
            if (data.status == 200) {
                dialogAlert("操作结果", "修改定位设置成功", "确定");
            } else {
                //显示错误提示信息
                $('#locationErrMsg').text(data.msg);
                $('#locationErrMsg').removeClass('hidden');
                $('#saveLoc').prop('disabled', false);
            }
        },
        error: function (err) {
            location.href = '/';
        }
    });
}
