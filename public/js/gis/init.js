/**
 * Created by zhoulanhong on 3/16/17.
 * 本类是前端web gis的初始脚本,所有的脚本都是从这里开始的
 */

//初始化地图
var m = new MapOperation(new BaiduMapAdapter());
m.initMap("allmap", true, true);

var client = null;

function initStallTree() {
    $("#organization").resizable();
    $("#organization").draggable({cursor: "move"});
    $("#commandModeDiv").draggable({cursor: "move"});
}

//初始化组织树
var gisOrgTree = new tree("orgtree", {
    async: {//同ajax设置
        enable: true,
        url: "/api/contact/organization",
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        type: "get"
    },
    check: {
        enable: true
    },
    //callback: {
    //    onClick: locateUser,
    //    onCheck: rep.selectStaff,
    //    //onNodeCreated: createHideStaffElement,
    //    onExpand: myOnExpand,
    //    beforeAsync: connectCommander,
    //    onAsyncSuccess: zTreeOnAsyncSuccess,
    //},
    data: {
        simpleData: {
            enable: true,
            idKey: '_id',
            pIdKey: 'father',
            rootPId: -1,
        }
    }
});