/**
 * Created by zhoulanhong on 3/21/17.
 * 本类用于处理地图上左边的那棵树以及相关的操作
 */
var onNums = 0;        //在线人员
var notOnlinNum = 0;   //离线人员
var nodesNum = 0;      //总人员
var onlineSet = new Set();
//设置在线状态
function setOnlieStaff(id) {
    $("#" + gisOrgTree.getTreeNodeById(id).tId + "_span").addClass("ztreeOnlieNameStyle");

    onlineSet.add(id + "");
    $('.personOnline span').text(onlineSet.size);

    notOnlinNum = nodesNum - onlineSet.size;
    $('.personNotOnline span').text(notOnlinNum);
}

//设置离线状态
function setOfflieStaff(id) {
    $("#" + gisOrgTree.getTreeNodeById(id).tId + "_span").removeClass("ztreeOnlieNameStyle");

    onlineSet.delete(id + "");
    $('.personOnline span').text(onlineSet.size);
    notOnlinNum = nodesNum - onlineSet.size;
    $('.personNotOnline span').text(notOnlinNum);
}

function showConversation() {
    $('#conversationMain').slideDown(1000);
}

function showUnread(nbr) {
    if (nbr > 0) {
        $('#talksRecord').html('<div class="newsRemid">' + nbr + '</div>会话记录');
    }
    else {
        $('#talksRecord').html('会话记录');
    }
}

//退出来登陆界面
function kickCurUser() {
    location.href = "/";
}

//当树展开时,显示在线状态
function myOnExpand() {
    onlineSet.forEach(function (key) {
        setOnlieStaff(key);
    });
}

var firstAsyncSuccessFlag = 0;
function zTreeOnAsyncSuccess(event, treeId, msg) {

    if (firstAsyncSuccessFlag == 0) {
        try {
            //调用默认展开第一个结点
            var nodes = gisOrgTree.getTree().getNodes();
            gisOrgTree.getTree().expandNode(nodes[1], true);
            var childNodes = gisOrgTree.getTree().transformToArray(nodes[0]);
            gisOrgTree.getTree().expandNode(childNodes[1], true);
            gisOrgTree.getTree().selectNode(childNodes[1]);
            firstAsyncSuccessFlag = 1;

            nodesNum = gisOrgTree.getAllLeafNodes().length;
            $('.allpersonBox span').text(nodesNum);
            //gisOrgTree.setLeafNodeStyle("ztreeNotOnlieNameStyle");

            notOnlinNum = nodesNum - onNums;
            $('.personNotOnline span').text(notOnlinNum);
        } catch (err) {

        }
    }
}

function showSelectCount(count) {
    $('.chosePerson span').text(count + ")");
}

function getPareNodes() {
    gisOrgTree.getPareNodes();
}
//定位树上点击的用户
function locateUser(event, treeId, treeNode) {

    //if (localFlag || !treeNode.lastLocation) return false;
    if (!treeNode.lastLocation) return false;
    var treeObj = $.fn.zTree.getZTreeObj("orgtree");
    treeObj.checkNode(treeNode, false, true, false);
    m.locate({
        id: treeNode._id,
        lat: treeNode.lastLocation.coordinates[0],
        lng: treeNode.lastLocation.coordinates[1],
        speed: "0.0KM/H",
        iconPath: treeNode.icon,
        name: treeNode.name,
        userid: treeNode.idNumber,
        lastLocationTime: treeNode.lastLocationTimeCST,
        position: treeNode.positionname
    });

    $.ajax({
        url: '/api/contact/locateUser',
        dataType: 'json',
        type: 'post',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({
            userid: treeNode.idNumber
        }),
        success: function (data) {

        },
        error: function (err) {

        }
    });

}