/**
 * Created by zhoulanhong on 11/16/16.
 */



//pttChatClient = new PTTChatClient({
//    PTTSDKServer: {
//        protocol: 'https',
//        host: 'localhost',
//        port: '3344',
//    }
//});

//function authenticate(func) {
//    $.get('/api/contact/commander', function (data) {
//        if (data && data.status == 200) {
//            if (data.data == null)
//            {
//                alert('查询指挥号出错,请联系管理员.');
//                return;
//            }
//            pttChatClient.authenticate(data.data.idNumber, data.data.password)
//                .then(function (token) {
//                    console.log("authenticate success");
//                    func();
//                }).catch(function (error) {
//                console.log("authenticate failed.." + error);
//            });
//        } else {
//            alert("查询指挥号出错,请联系管理员.");
//        }
//    });
//
//}

function closeMessageWin(){
    pttChatClient.closeMessageWin();
}

function syncCall() {
    var phoneNumbers = [];
    if (selectedStaffs.size > 0) {
        selectedStaffs.forEach(function (staff) {
            phoneNumbers.push(staff.phoneNbr);
            pttChatClient.maximum();
        });
    }
    else {
        phoneNumbers.push('nobody');
    }

    authenticate(function testCa() {
        pttChatClient.call(phoneNumbers);
    });
}


function intercomCall(type) {

    if(pttChatClient == null)
    {
        alert('未登录通信服务器,请联系管理员');
        return;
    }
    var phoneNumbers = [];
    var treeObj = gisOrgTree.getTree();
    selectedStaffs = treeObj.getCheckedNodes();

    if (selectedStaffs.length > 0) {
        if (type == 1 || type == 2 || type == 4) {
            selectedStaffs.forEach(function (staff) {
                if (staff.phoneNbr != null) {
                    phoneNumbers.push(staff.phoneNbr);
                } else {
                    return;
                }
            });
            if (type != 4) {
                pttChatClient.maximum();
            }
        }
    }

    if (phoneNumbers.length <= 0)
    {
        alert("请从组织树上选择人员");
        return;
    }

    //authenticate(function testCa() {
        pttChatClient.call(phoneNumbers, type);
    //});
}


function testLeaveRoom() {
    exitRoomResult.innerHTML = "退出房间中...";
    pttChatClient.leaveRoom(true)
        .then(function () {
            exitRoomResult.innerHTML = "退出房间成功";
        })
        .catch(function (error) {
            exitRoomResult.innerHTML = "退出房间失败, 原因:" + JSON.stringify(error);
        });
}

function testControlMic() {
    exitRoomResult.innerHTML = "抢麦中...";
    pttChatClient.controlMic()
        .then(function () {
            controlMicResult.innerHTML = "抢麦成功";
        })
        .catch(function (error) {
            controlMicResult.innerHTML = "抢麦失败,原因:" + JSON.stringify(error);
        });
}

function testReleaseMic() {
    exitRoomResult.innerHTML = "释放中...";
    pttChatClient.releaseMic()
        .then(function () {
            releaseMicResult.innerHTML = "释放麦成功"
        })
        .catch(function (error) {
            releaseMicResult.innerHTML = "释放麦失败,原因:" + JSON.stringify(error);
        });
}

//syncCall();