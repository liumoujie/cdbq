/**
 * Created by 123456 on 2017/2/18.
 */
$(function () {
    var ok1 = false;
    var ok2 = false;

    $('#indexBody').keypress(function (key) {
        if (key.charCode == 13) {
            $('input[name="username"]').blur()
            $('input[name="password"]').blur();
            $('.loginBtns').click();
        }
    });

    $('.loginBtns').click(function () {
        $('input[name="username"]').blur()
        $('input[name="password"]').blur();
        if (!$('input[name="username"]').val()) {
            $('#erroT').text('用户名不能为空').removeClass('state2').addClass('state1').addClass('erroText');
            return false;
        }
        if (!$('input[name="password"]').val()) {
            $('#erroT').text('密码不能为空').removeClass('state2').addClass('state1').addClass('erroText');
            return false;
        }

        if (ok1 && ok2) {
            $('#erroT').text('正在提交......').removeClass('state1').addClass('state2').addClass('erroText');
            $('.formbox').submit();
        } else {
            return false;
        }
    });
    // 验证用户名
    $('input[name="username"]').focus(function () {
        $('#erroT').text('用户名应该为3-20位之间').removeClass('state1').addClass('erroText').addClass('state2');
    }).blur(function () {
        //if ($(this).val().length >= 3 && $(this).val().length <= 12 && $(this).val() != '') {
        //     $('#erroT').text('输入企业编号成功').removeClass('state1').addClass('state2');
            ok1 = true;
        //} else if ($(this).val() == '0') {
        //    $('#erroT').text('输入企业编号成功').removeClass('state1').addClass('state2');
        //    ok1 = true;
        //} else {
        //    $('#erroT').text('用户名应该为3-20位之间').removeClass('state2').addClass('state1');
        //}

    });

    //验证密码
    $('input[name="password"]').focus(function () {
        $('#erroT').text('密码应该为6-18位之间').removeClass('state1').addClass('erroText').addClass('state2');
    }).blur(function () {
        if ($(this).val().length >= 6 && $(this).val().length <= 18 && $(this).val() != '') {
            // $('#erroT').text('密码输入成功').removeClass('state1').addClass('state2');
            ok2 = true;
        } else {
            $('#erroT').text('密码应该为6-18位之间').removeClass('state2').addClass('state1');
        }

    });
});