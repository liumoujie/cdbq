/**
 * Created by zhoulanhong on 2/18/17.
 * 本类用于处理点相关的一些操作,目前包括两大方法:
 *  1,计算一个角在另外一个角的多少度方向
 *  2,在两个点之间,根据自己想要的距离进行补点
 */

function calcAngle(p1, p2) {
    //先把两个点换算为相对坐标,将第一个点设置为0,0;
    //这个值为 360/PI ,是一个测算弧度的常量

    p2 = {x: p2.x - p1.x, y: p1.y - p2.y};
    p1 = {x: 0, y: 0};

    //同一个点
    if (p1.x == p2.x && p1.y == p2.y) {
        return 0
        //在同一垂直线上
    } else if (p1.x == p2.x) {
        return p2.y > 0 ? 0 : 180;
        //在同一水平线上
    } else if (p1.y == p2.y) {
        return p2.x > 0 ? 90 : 270;
    } else {
        var gou = Math.abs(p2.x - p1.x);
        var gu = Math.abs(p2.y - p1.y);
        var xian = Math.sqrt(gou * gou + gu * gu);
        var si = gu / xian;
        var degree = (Math.asin(si) / 0.017453293).toFixed(0);
        switch (getArea(p1, p2)) {
            case 1 :
                return 90 - degree;
            case 2:
                return 270 + parseInt(degree);
            case 3:
                return 270 - parseInt(degree);
            case 4:
                return 90 + parseInt(degree);
            default:
                return 0;
        }
    }
}

/*获取每一个点相对于另一个点的象限
 *                     |
 *                     |
 *          2          |          1
 *                     |
 * ----------------------------------------
 *                     |
 *                     |
 *          3          |          4
 *                     |
 * */
function getArea(p1, p2) {
    if (p2.y > p1.y) {
        return p2.x > p1.x ? 1 : 2;
    } else {
        return p2.x > p1.x ? 4 : 3;
    }
}

// 子方法， 在太长的线上补充点
function insertPixelsForLong(p1, p2) {
    var ps = [];
    console.log(JSON.stringify(p1) + JSON.stringify(p2));
    var pix1 = m.pointToPixels(p1.lng, p1.lat);
    var pix2 = m.pointToPixels(p2.lng, p2.lat);
    console.log(JSON.stringify(pix1) + "  " + JSON.stringify(pix2));
    insertForTwoPixels(pix1, pix2, ps);
    return ps.length >= 2 ? ps : [];
}

// p1 和p2 之间增加点
function insertForTwoPixels(p1, p2, ps) {
    ps[ps.length] = p1;

    var dx = p2.x - p1.x;
    var dy = p2.y - p1.y;

    var L = Math.sqrt(dy * dy + dx * dx);

    var sin = dy / L;
    var cos = dx / L;

    var count = parseInt(L / 3), px, py;

    //把第一个点加入到数组当中
    // ps.push(new BMap.Pixel(p1.x, p1.y));

    for (var i = 1; i <= count; ++i) {
        px = parseInt(p1.x + cos * 3 * i);
        py = parseInt(p1.y + sin * 3 * i);

        ps.push(new BMap.Pixel(px, py));
    }

    //把最后一个点加入到数组当中
    ps.push(new BMap.Pixel(p2.x, p2.y));
}