/**
 * Created by zhoulanhong on 12/18/16.
 * 所有的全局变量,也将定义在这个文件当中
 * 这个类里面,将会写一些公共的方法
 */

// 用于保存所有的在线人员,当下线时,从这个map中移除,
onlineMap = new Map();
// 用于保存页面上所选择的员工的信息
selectedStaffs = new Set();
// 地图,
map = null;
// 服务器配置
SERV = null;

// web对讲服务器配置
pttChatClient = null;

function getTimeByDate(dateTime) {
    var d = moment(dateTime);
    return d.utc();
}

/*
 * 初始化界面,包括所有的服务器地址等相关的内容
 *
 * */
function init(page) {
    if (page == "gis") {
        $.get('/admin/server', function (data) {

            console.log("-- query server :" + JSON.stringify(data));

            if (data && data.status == 200) {
                SERV = data.url;
                //alert("https://"+SERV.signal_host+":"+SERV.signal_port);
                new ScheduleWSClient("https://"+SERV.signal_host+":"+SERV.signal_port);
                //new ScheduleWSClient("https://localhost:8444");
                pttChatClient = new PTTChatClient({
                    PTTSDKServer: {
                        protocol: 'https',
                        host: SERV.ptt_web_host,
                        port: SERV.ptt_web_port,
                    }
                });
                //alert(pttChatClient);
            } else {
                alert("初始化系统出错");
            }
        });
    }
}

function dialogConfirm(title, msg, cnfFun) {

    BootstrapDialog.confirm({
        title: title,
        message: msg,
        type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
        closable: true, // <-- Default value is false
        draggable: true, // <-- Default value is false
        btnCancelLabel: '取消', // <-- Default value is 'Cancel',
        btnOKLabel: '确定', // <-- Default value is 'OK',
        btnOKClass: 'btn-warning', // <-- If you didn't specify it, dialog type will be used,
        callback: function (result) {
            // result will be true if button was click, while it will be false if users close the dialog directly.
            console.log("--- callbac before :" + result);
            cnfFun(result);
        }
    });
}

function dialogAlert(title, msg, btn) {
    BootstrapDialog.show({
        title: title,
        message: msg,
        buttons: [{
            label: btn,
            action: function (dialog) {
                dialog.close();
            }
        }]
    });
}