/**
 * Created by zhoulanhong on 12/29/16.
 */

var showFlag = false;
var lastTreeId = "";
var locateTree;
var locSelectedStaffs = new Set();

var settingOrganizationStaff = {
    async: {//同ajax设置
        enable: true,
        url: "/api/contact/organization",
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        type: "get"
    },
    check: {
        enable: true
    },
    callback: {
        // onClick: locateUser,
        onCheck: selectStaff,
        onAsyncSuccess: locatezTreeOnAsyncSuccess
    },
    data: {
        simpleData: {
            enable: true,
            idKey: '_id',
            pIdKey: 'father',
            rootPId: -1,
        }
    }
};

function selectStaff(event, treeId, curNode) {

    locSelectedStaffs.clear();
    var treeNode = curNode;
    var allSelectedStaff = "";
    //var treeObj = $.fn.zTree.getlocateTree(treeId);
    var treeObj = locateTree.getTree();
    var checkedNodes = treeObj.getCheckedNodes();
    for (var i in checkedNodes) {
        // 不是叶子节点
        if (!checkedNodes[i].isParent) {
            allSelectedStaff += checkedNodes[i].name + " ";
            locSelectedStaffs.add(
                {
                    "_id": checkedNodes[i]._id,
                    "idNumber": checkedNodes[i].idNumber,
                    "phoneNbr": checkedNodes[i].phoneNbr,
                    "staffName": checkedNodes[i].name
                }
            );
        }
    }
    $('#locateSelected').text(allSelectedStaff);

    if (locSelectedStaffs.size > 1 ||
        locSelectedStaffs.size == 0)
    {//选择多人,使用上一次选择人员的配制,以便将设置应用到选择的所有人
        return;
    }

    if (!treeNode.checked)
    {
        if (locSelectedStaffs.size == 1)
        {//只有一个选中时,显示其配制
            treeObj.getCheckedNodes().forEach(function(it){
                if (!it.isParent)
                {
                    treeNode = it;
                }
            }
            );
        }else
        {
            return;
        }
    }

    //设置立即生效
    if (treeNode.locationEnable)
    {
        $('#locationEnable').prop('checked', true);
    }else {
        $('#locationEnable').prop('checked',false);
    }

    //回显周几天周几
    if (treeNode.locationWeekly) {
        var weeklyCheckbox = ["setMon", "setQue", "setWen", "setThu", "setFri", "setSat", "setSun"];
        var idx = 0;
        var cnt = 0;
        treeNode.locationWeekly.forEach(function (it) {
            if (it)
            {
                //$('#' + weeklyCheckbox[idx]).attr('checked', true).show;
                $('#' + weeklyCheckbox[idx]).prop('checked', true);
                cnt ++;
            }else {
                $('#' + weeklyCheckbox[idx]).prop('checked',false);
            }
            idx++;

            if (cnt == treeNode.locationWeekly.length)
            {
                $("#setall").prop('checked', true);
            }else
            {
                $("#setall").prop('checked', false);
            }
        });
    }

    //回显开始和持继时间
    if (treeNode.locationTime) {
        $('#setStartTime').val(treeNode.locationTime.from);
        $('#lastTime').val(treeNode.locationTime.last);
    }

    //定位上传频率
    if (treeNode.locationReportInterval == treeNode.locationScanInterval)
    {
        $("input[type=radio][name='locateUpd'][value='0']").prop('checked', true);
    }
    else
    {
        $("input[type=radio][name='locateUpd'][value=" + treeNode.locationReportInterval + "]").prop('checked', true);
    }

    //定位频率
    if (treeNode.locationScanInterval <= 300) {
        $("input[type=radio][name='locateFeq'][value=" + treeNode.locationScanInterval + "]").prop('checked', true);
    } else {
        $("input[type=radio][name='locateFeq'][value='0']").prop('checked', true);
        $('#setLocateFeq').val(treeNode.locationScanInterval / 60);
    }
}

function locateUser(event, treeId, treeNode) {
    $('#' + treeNode.tId + "_check").click();
}

function initLocateSetTree() {
    locateTree = new tree("orgLocTree", settingOrganizationStaff);
}

function selectCustom() {
    $("input[name='locateUpd']:eq(5)").click();
}

function locatezTreeOnAsyncSuccess(event, treeId, treeNode, msg)
{
    var cnt = 0;
    locateTree.getTree().getNodesByFilter(function (node) {

        if (cnt == locSelectedStaffs.size)
        {
            return false;
        }

        locSelectedStaffs.forEach(function(it){
            if (it._id == node._id)
            {
                locateTree.getTree().checkNode(node,true,true);
                cnt++;

                ////子结点符合条件时,显示其所有父结点
                //var curNode = node;
                //do {
                //    var parentNode = curNode.getParentNode();
                //    if (null == parentNode) {
                //        break;
                //    }
                //    //父结点已是显示的就不用再往上找父了
                //    if (!parentNode.isHidden) {
                //        break;
                //    }
                //    locateTree.getTree().showNode(parentNode);
                //    curNode = parentNode;
                //} while (true);
                //return true;
            }
        });

        return false;
    },false);

}

function saveLoc() {
    $('#locationErrMsg').addClass('hidden');
    var locateFeq = $('input:radio[name="locateFeq"]:checked').val();
    var locationEnable = true;
    if (!locateFeq) {
        //$('#locationErrMsg').text('请选择定位频率');
        //$('#locationErrMsg').removeClass('hidden');
        //return;
        locationEnable = false;
    }
    if (locateFeq == 0) {
        var setLocateFeq = $('#setLocateFeq').val();
        if (!setLocateFeq) {
            $('#locationErrMsg').text('自定义定位频率,请输入时间');
            $('#locationErrMsg').removeClass('hidden');
        }
        locateFeq = setLocateFeq * 60;
    }

    var locateUpd = $('input:radio[name="locateUpd"]:checked').val();
    if (!locateUpd) {
        //$('#locationErrMsg').text('请选择上传频率');
        //$('#locationErrMsg').removeClass('hidden');
        //return;
        locationEnable = false;
    }
    if (locateUpd == 0) {
        locateUpd = locateFeq;
    }

    var selStaffs = [];
    locSelectedStaffs.forEach(function (staff) {
        //selStaffs += selStaffs.length > 0 ? ("," + staff._id ) : staff._id;
        selStaffs.push(staff.idNumber);
    });
    var weekly = [
        $('#setMon').is(':checked') ? 1 : 0,
        $('#setQue').is(':checked') ? 1 : 0,
        $('#setWen').is(':checked') ? 1 : 0,
        $('#setThu').is(':checked') ? 1 : 0,
        $('#setFri').is(':checked') ? 1 : 0,
        $('#setSat').is(':checked') ? 1 : 0,
        $('#setSun').is(':checked') ? 1 : 0
    ];
    if (!(weekly[0] || weekly[1] || weekly[2] || weekly[3] || weekly[4] || weekly[5] || weekly[6])) {
        //$('#locationErrMsg').text('至少选择一天');
        //$('#locationErrMsg').removeClass('hidden');
        //return;
        locationEnable = false;
    }

    var st = $('#setStartTime').val();
    var et = $('#lastTime').val();
    if (!(st && et)) {
        //$('#locationErrMsg').text('开始时间和结束时间必填');
        //$('#locationErrMsg').removeClass('hidden');
        //return;
        locationEnable = false;
    }

    if (!locSelectedStaffs || locSelectedStaffs.size == 0) {
        $('#locationErrMsg').text('请选择人员');
        $('#locationErrMsg').removeClass('hidden');
        return;
    }

    if ($('#lastTime').val() < 1 || $('#lastTime').val() > 24) {
        $('#locationErrMsg').text('持续时间必须在1到24小时之间');
        $('#locationErrMsg').removeClass('hidden');
        return;
    }

    if ($('#locationEnable').is(':checked') && locationEnable) {
        locationEnable = true;
    } else {
        locationEnable = false;
    }

    $('#saveLoc').prop('disabled', true);
    $.ajax({
        url: '/api/contact/updUserLocateSet',
        dataType: 'json',
        type: 'post',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({
            idNumber: selStaffs,
            locationWeekly: weekly,
            locationTime: {
                from: $('#setStartTime').val(),
                last: $('#lastTime').val()
            },
            locationScanInterval: locateFeq,
            locationReportInterval: locateUpd,
            locationEnable: locationEnable
        }),
        success: function (data) {
            //成功
            if (data.status == 200) {
                finishAlert("操作结果", "修改定位设置成功", "确定");
                //locateTree.getTree().refresh();
                locateTree.getTree().reAsyncChildNodes(null, "refresh");
            } else {
                //显示错误提示信息
                $('#locationErrMsg').text(data.msg);
                $('#locationErrMsg').removeClass('hidden');
                $('#saveLoc').prop('disabled', false);
            }
        },
        error: function (err) {
            location.href = '/';
        }
    });

}


/**
 *
 * @param inputID, String, input输入框id,
 */
function searchInLocTree(inputID) {
    var treeObj = locateTree.getTree();
    var condition = $('#' + inputID).val();
    if (condition.length == 0) {//为提升效率
        treeObj.showNodes(treeObj.getNodesByParam("isHidden", true));
        return;
    }

    var zNodes = treeObj.getNodesByFilter(function (node) {
        if (node.name.indexOf(condition) == -1) {
            treeObj.hideNode(node);
            return false;
        } else {
            //子结点符合条件时,显示其所有父结点
            var curNode = node;
            do {
                var parentNode = curNode.getParentNode();
                if (null == parentNode) {
                    break;
                }
                //父结点已是显示的就不用再往上找父了
                if (!parentNode.isHidden) {
                    break;
                }
                treeObj.showNode(parentNode);
                curNode = parentNode;
            } while (true);
            //修改前的搜索条件时可能会让当前结点已处于hide状态,所以要显示
            treeObj.showNode(node);
        }
        return true;
    },false);
}
