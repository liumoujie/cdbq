/**
 * Created by 123456 on 2017/5/7.
 */
var showFlag = false;
var lastTreeId = "";
var userNearbyTree;
var userNearbySelectedStaffs = new Set();
var settingNearbyUserOrganizationStaff = {
    async: {//同ajax设置
        enable: true,
        url: "/api/contact/organization",
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        type: "get"
    },
    check: {
        enable: true
    },
    callback: {
        onCheck: selectNearbyStaff
    },
    data: {
        simpleData: {
            enable: true,
            idKey: '_id',
            pIdKey: 'father',
            rootPId: -1,
        }
    }
};
function selectNearbyStaff(event, treeId, treeNode) {
    userNearbySelectedStaffs = new Set();
    var allSelectedStaff = "";
    var treeObj = userNearbyTree.getTree();
    var checkedNodes = treeObj.getCheckedNodes();
    for (var i in checkedNodes) {
        // 不是叶子节点
        if (!checkedNodes[i].isParent) {
            allSelectedStaff += checkedNodes[i].name + " ";
            userNearbySelectedStaffs.add(
                {
                    "_id": checkedNodes[i]._id,
                    "idNumber": checkedNodes[i].idNumber,
                    "phoneNbr": checkedNodes[i].phoneNbr,
                    "staffName": checkedNodes[i].name
                }
            );
        }
    }
    $('#userNearbySelected').text(allSelectedStaff);
}

function initNearbyLocateSetTree() {
    userNearbyTree = new tree("orgNearbyUserTree", settingNearbyUserOrganizationStaff);
}

function selectCustom() {
    $("input[name='locateUpd']:eq(5)").click();
}

//通过id数组,将数组内的成员勾选上
function checkNearbyTreeNodeById(ids) {
    return userNearbyTree.checkNodeByIds(ids, "idNumber", "name");
}

function saveNearbyUser() {
    $('#userErrMsg').addClass('hidden');
    var selStaffs = [];
    userNearbySelectedStaffs.forEach(function (staff) {
        selStaffs.push(staff.idNumber);
    });

    if ((!userNearbySelectedStaffs || userNearbySelectedStaffs.size == 0) && $('#userNearbySelected').text().length == 0) {
        $('#userErrMsg').text('请选择人员');
        $('#userErrMsg').removeClass('hidden');
        return;
    }

    $('#viewMap').prop('checked', true);
    $('#btnUserZtreeSave').prop('disabled', true);
    $('#nearbyUsers').val(selStaffs);
    $('.dlgAddUserZtree').hide();
}
/**
 *
 * @param inputID, String, input输入框id,
 */
function searchInLocTree2(inputID) {
    var condition = $('#' + inputID).val();
    if (condition.length == 0) {//为提升效率
        userTree.showNodes(userTree.getNodesByParam("isHidden", true));
        return;
    }
    var zNodes = userTree.getNodesByFilter(function (node) {
        if (node.name.indexOf(condition) == -1) {
            userTree.hideNode(node);
            return false;
        } else {
            //子结点符合条件时,显示其所有父结点
            var curNode = node;
            do {
                var parentNode = curNode.getParentNode();
                if (null == parentNode) {
                    break;
                }
                //父结点已是显示的就不用再往上找父了
                if (!parentNode.isHidden) {
                    break;
                }
                userTree.showNode(parentNode);
                curNode = parentNode;
            } while (true);
            //修改前的搜索条件时可能会让当前结点已处于hide状态,所以要显示
            userTree.showNode(node);
        }
        return true;
    }, false);


}
