/**
 *
 * Created by suke on 15/12/8.
 *
 * 唯一号码生成系统
 *
 */

var mongoose = require('mongoose');

var PttNumberGenneratorSchema = new mongoose.Schema(
    {
        //自增长数字的分类 [-1固定为分类的的自增长类型, -2为房间号码类型]
        _id: Number,

        //自增长数字
        seq: Number
    },
    {
        collection: "counters" //给collections指定一个名字，而不是系统自动生成
    }
);

PttNumberGenneratorSchema.statics = {

    //返回下一个企业号
    getNextEnterpriseNumber: function (cb) {

        return PttNumberGennerator.findByIdAndUpdate(-1, {$inc: {seq: 1}}, {new: true, upsert: true}, cb);

    },

    //生成某个企业下的用户号码
    getNextContactUserNumber: function (enterpriseNumber, cb) {
        return PttNumberGennerator.findByIdAndUpdate(enterpriseNumber, {$inc: {seq: 1}}, {new: true, upsert: true}, cb);
    },

    //批量生成某个企业下的用户号码，用于用户导入
    getBatchNextContactUserNumber: function (enterpriseNumber, increase, cb) {
        return PttNumberGennerator.findByIdAndUpdate(enterpriseNumber, {$inc: {seq: increase}}, {
            new: true,
            upsert: true
        }, cb);
    },

    //生成某个企业下的通讯群组号码
    getNextContactGroupNumber: function (enterpriseNumber, cb) {
        return PttNumberGennerator.findByIdAndUpdate(enterpriseNumber + 1, {$inc: {seq: 1}}, {new: true, upsert: true}, cb);
    },

    //批量生成某个企业下的通讯群组号码
    getBatchNextContactGroupNumber: function (enterpriseNumber, increase, cb) {
        return PttNumberGennerator.findByIdAndUpdate(enterpriseNumber + 1, {$inc: {seq: increase}}, {new: true, upsert: true}, cb);
    },

    //生成某个企业下的对话房间的号码
    getNextChatRoomNumber: function (cb) {
        return PttNumberGennerator.findByIdAndUpdate(-2, {$inc: {seq: 1}}, {new: true, upsert: true}, cb);
    }
}

PttNumberGenneratorSchema.methods = {}


var PttNumberGennerator = mongoose.model("PttNumberGennerator", PttNumberGenneratorSchema);

module.exports = PttNumberGennerator;