/**
 * Created by zhoulanhong on 4/26/17.
 * 用于保存地图上的标记
 */
var mongoose = require('mongoose');

var CdbqExportSchema = new mongoose.Schema(
    {
        //单据时间
        expTime: String,

        //单据类型
        expType: String,

        //单据清单
        billNumber: String,

        //文件名
        fileName: String,

        //保存位置
        repPath: String,

        //下载路径
        downLoadPath: String,
    },
    {
        collection: "cdbqExport" //给collections指定一个名字，而不是系统自动生成
    }
);

CdbqExportSchema.statics = {
}

CdbqExportSchema.methods = {}


var CdbqExport = mongoose.model("CdbqExport", CdbqExportSchema);

module.exports = CdbqExport;