/**
 * Created by suke on 15/12/5.
 *
 * 企业的模型定义
 */
var mongoose = require('mongoose');

var PttEnterpriseSchema = new mongoose.Schema(
    {
        //所属企业,xp.qing adding for gis,如:集团公司
        enterprise: {type: mongoose.Schema.ObjectId, ref: "PttEnterprise"},

        //上级,可以是企业,部门,也可以是人.用于保存组织关系.xp.qing added for gis
        father: {type: mongoose.Schema.ObjectId},

        //企业名称
        name: String,

        //企业名称
        phoneNumber: Number,

        //企业号码
        idNumber: Number,

        //过期时间
        expTime: Number,

        //企业的LOGO
        //logo: String,

        //企业的配额
        quota: {
            //员工最大限制
            staff: {type: Number, default: 500},

            //预定义组用户数限制
            preGroup: {type: Number, default: 100},

            //用户自定义组的用户数限制
            //preGroupPerson: {type: Number, default: 30}
        },

        //管理账号密码
        managerPwd: String,

        //通讯录版本
        version: {type: Number, default: 0},

        //拥有的权限
        privileges: [String],

        //当前企业拥有的密钥
        key: String,

        //当前企业拥有的密钥
        vedioMode: String,

        //当前模型
        mode: {
            modetype: String, //0 是观察   1 是指挥
            commandTime: Number,
            collectFreq: Number,
            endTime: Date
        }
    },
    {
        collection: "enterprises" //给collections指定一个名字，而不是系统自动生成
    }
);

PttEnterpriseSchema.statics = {
    //listByUser: function (userId, cb)
}

PttEnterpriseSchema.methods = {}


var PttEnterprise = mongoose.model("PttEnterprise", PttEnterpriseSchema);

module.exports = PttEnterprise;