/**
 * Created by fanchao on 12/11/16.
 *
 * 提供日志审查模型
 */

var mongoose = require('mongoose');

var PttAuditSchema = new mongoose.Schema(
    {
        // 事件
        eventName: {type: String, index: true},

        // 事件发生时间
        time: {type: Date},

        // 其它属性（任意指定）
        attrs : {type: Object}
    },
    {
        collection: "audits" //给collections指定一个名字，而不是系统自动生成
    }
);

var PttAudit = mongoose.model("PttAudit", PttAuditSchema);

module.exports = PttAudit;