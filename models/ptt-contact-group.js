/**
 * Created by suke on 15/12/6.
 * 通讯录群组的模型定义
 */

var mongoose = require('mongoose');

var PttContactGroupSchema = new mongoose.Schema(
    {
        //所属企业
        enterprise: {type: mongoose.Schema.ObjectId, ref: "PttEnterprise"},

        //群ID号
        idNumber: {type: Number, index: true},

        //群名字
        name: String,

        //群描述
        description: String,

        //群成员
        members: [Number]
    },
    {
        collection: "contact.groups" //给collections指定一个名字，而不是系统自动生成
    }
);

var PttContactGroup;

PttContactGroupSchema.statics = {
    /**
     * 查找用户所在的所有群组
     *
     * @param userId {Number} 用户ID
     * @return {Promise<Array<PttContactGroup>>}
     */
    listByUser: function (userId) {
        return PttContactGroup.find({ members: userId })
    }
};

PttContactGroupSchema.methods = {}

PttContactGroup = mongoose.model("PttContactGroup", PttContactGroupSchema);
module.exports = PttContactGroup;