/**
 * Created by zhoulanhong on 4/26/17.
 * 用于保存地图上的标记
 */
var mongoose = require('mongoose');

var PttMarkerSchema = new mongoose.Schema(
    {
        //所属企业
        enterprise: {type: mongoose.Schema.ObjectId, ref: "PttEnterprise"},

        //名称
        name: String,

        //描述
        description: String,

        //类型
        type: String,

        //此标记在地图上的位置
        location:{
            lng:String,
            lat:String
        },

        //标记的图片信息
        images: []
    },
    {
        collection: "marker" //给collections指定一个名字，而不是系统自动生成
    }
);

PttMarkerSchema.statics = {
}

PttMarkerSchema.methods = {}


var PttMarker = mongoose.model("PttMarker", PttMarkerSchema);

module.exports = PttMarker;