/**
 * Created by zhoulanhong on 10/5/16.
 * for user location
 */
var mongoose = require('mongoose');

var PrivilegeSchema = new mongoose.Schema(
    {
        //编码
        code: String,

        //名称
        name: String,

        //描述
        description: String,

    },
    {
        collection: "privilege" //给collections指定一个名字，而不是系统自动生成
    }
);

PrivilegeSchema.statics = {
    //listByUser: function (userId, cb)
}

PrivilegeSchema.methods = {}


var PttPrivilege = mongoose.model("PrivilegeSchema", PrivilegeSchema);

module.exports = PttPrivilege;