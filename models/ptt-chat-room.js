/**
 * Created by suke on 15/12/6.
 * 语音房间的模型定义
 */

var mongoose = require('mongoose');
const PttContactGroup = require('./ptt-contact-group');
const co = require('co');
const _ = require('underscore');

var PttChatRoomSchema = new mongoose.Schema(
    {
        //房间ID号
        idNumber: Number,

        //所属企业
        enterpriseId: Number,

        //房间名字
        name: {type: String, index: true},

        //与房间相关的组
        associatedGroups: [Number],

        //除了组以外的其它成员用户
        extraMembers: [Number],

        //房间的管理员及所有者,只有一个用户.
        owner: Number,

        //是不是指挥房间,如果是,则需要强制所有的人进入该房间会话,在会话界面也会一直处在最开始的位置,则有明显的标识.
        important: {type: Boolean, default: false},

        //是否允许被邀请人在邀请人退出后加入房间
        allowInviteesToJoinEmptyRoom: {type: Boolean, default: false}
    },
    {
        collection: "chat.rooms" //给collections指定一个名字，而不是系统自动生成
    }
);

var PttChatRoom;

PttChatRoomSchema.statics = {
    /**
     * 查找用户所在的所有聊天室
     *
     * @param userId {Number}
     * @return {Promise<Array<PttChatRoom>>}
     */
    listByUser: function (userId) {
        return co(function *() {
            // Find all the groups user belong to...
            const groupIds = _.map(yield PttContactGroup.listByUser(userId), group => group.idNumber);

            const roomsContainingGroups = yield PttChatRoom.find({associatedGroups: {$in: groupIds}});
            const roomsContainingMembers = yield PttChatRoom.find({extraMembers: userId});

            return _.union(roomsContainingGroups, roomsContainingMembers);
        });
    }
};

PttChatRoomSchema.methods = {

    /**
     * 转换为客户端认识的输出
     */
    toResponse: function () {
        "use strict";

        console.log("-------------- this.roomMode " + this.roomMode);

        return {
            idNumber: this.idNumber,
            name: this.name,
            ownerId: this.owner,
            associatedGroupIds: this.associatedGroups ? this.associatedGroups : [],
            extraMemberIds: this.extraMembers ? this.extraMembers : [],
            roomMode: this.roomMode
        }
    }
};

PttChatRoom = mongoose.model("PttChatRoom", PttChatRoomSchema);

module.exports = PttChatRoom;