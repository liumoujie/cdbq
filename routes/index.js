var express = require('express');
var co = require('co');

var pttAuth = require('../common/ptt-auth');
var PttEnterprise = require('../models/ptt-enterprise');

var appUrl = process.env.PROD_APP_URL || 'http://'+process.env['SIGNAL_SERVER_URL']+':10005/app/latest/';

var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('index', {titlevar: 'Express', appUrl: appUrl, version: process.env['VERSION']});
});

/**
 * 管理系统
 */
router.get('/admin', pttAuth.isAuthenticated, function (req, res, next) {
    co(function*() {
        var enterprise = req.user;

        if (!enterprise) {

            res.render('login', {status: 500, errorMsg: "未知错误,请联系管理员."});

        } else {

            if (enterprise.idNumber === 0) {
                res.render('root', {appUrl: appUrl});
            } else {

                res.render('admin', {
                    user: req.user,
                    enterprise: enterprise,
                    appUrl: appUrl,
                    img_host: process.env['FDFS_IMAGE_SERV_URL'],
                    img_port: process.env['FDFS_IMAGE_SERV_PORT']
                });
            }

        }
    });
});

module.exports = router;
