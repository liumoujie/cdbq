var express = require('express');
var router = express.Router();
var _ = require('lodash');
var pttAuth = require('../common/ptt-auth');

var appUrl = process.env.PROD_APP_URL || 'http://'+process.env['SIGNAL_SERVER_URL']+':10005/app/latest/';

module.exports = function (passport) {

    router.get('/login', function (req, res, next) {
        res.render('index', {
            status: 500,
            appUrl: appUrl,
            version: process.env['VERSION']
        });
    });

    router.post('/login', function (req, res, next) {

        if (!req.body.username)
            return res.render("index", {
                status: 500,
                errorMsg: '企业编号不能为空',
                appUrl: appUrl,
                version: process.env['VERSION']
            });

        console.log(_.toNumber(req.body.username));
        if (_.isNaN(_.toNumber(req.body.username))) {
            return res.render("index", {
                status: 500,
                errorMsg: '企业编号必须为数字',
                appUrl: appUrl,
                version: process.env['VERSION']
            });
        }

        if (!req.body.password)
            return res.render("index", {
                status: 500,
                errorMsg: '密码不能为空',
                appUrl: appUrl,
                version: process.env['VERSION']
            });

        passport.authenticate('login', function (err, user, info) {
            //console.log(">>>> err  = ", err);
            //console.log(">>>> user = ", user);
            //console.log(">>>> info = ", info);

            //取出错误信息
            var message = null;
            var messageAry = req.flash('message');
            if (messageAry && messageAry.length > 0) {
                message = messageAry[0];
            }

            if (err) {
                return next(err);
            }

            if (!user) {
                //return res.render("login", {
                //    status: 500,
                //    errorMsg: message
                //})

                return res.render("index", {
                    status: 500,
                    errorMsg: message,
                    appUrl: appUrl,
                    version: process.env['VERSION']
                });

            } else {
                req.logIn(user, function (err) {
                    if (err) {
                        return next(err);
                    }
                    if (user.idNumber == "0" || _.indexOf(user.privileges, "gisManage") == -1) {
                        return res.redirect('/admin');
                    } else {
                        return res.redirect('/gis');
                    }
                });
            }
        })(req, res, next);
    });

    router.get('/logout', function (req, res, next) {
        req.logout();
        res.redirect('/');
    });

    router.get('/server', pttAuth.isAuthenticated, function (req, res, next) {
        res.send({
            status: 200, url: {
                "signal_host": process.env.SIGNAL_SERVER_URL,
                "signal_port": process.env.SIGNAL_SERVER_SSL_PORT,
                "ptt_web_host": process.env.PTT_WEB_SERVER_URL,
                "ptt_web_port": process.env.PTT_WEB_PORT,
                "fdfs_img_host": process.env.FDFS_IMAGE_SERV_URL,
                "fdfs_img_port": process.env.FDFS_IMAGE_SERV_PORT
            }
        });
    });

    return router;
}
