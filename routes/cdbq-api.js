/**
 * Created by zhoulanhong on 1/16/18.
 */
var express = require('express');
var router = express.Router();
var pttAuth = require('../common/ptt-auth');
var CdbqExport = require('../models/cdbq-export');
var co = require('co');
let kdSession = require('../service/SessionService');
let queryDetailSerivce = require('../service/QueryBillDetail');
let DealInStock = require('../service/DealInStock');
var cmoment = require('moment');

// 查询导出详情
router.get('/queryExports', pttAuth.isAuthenticated, function (req, res, next) {
    co(function*() {

        //查找企业下的所有成员
        var data = yield CdbqExport.find().exec();

        //输出结果
        res.json({'status': 200, 'data': data});

    }).catch(function (err) {
        console.log(err);
        return res.json({status: 500, message: err.message});
    });
});

// 查询导出详情
router.get('/export', pttAuth.isAuthenticated, function (req, res, next) {
    co(function*() {
        console.log(req.query.billType);
        console.log(req.query.billNumber);

        var exist = yield CdbqExport.findOne({billNumber: req.query.billNumber}).exec();
        if(exist){
            res.json({'status': 200, 'data': req.query.billNumber});
            return null;
        }

        //获取单据数据
        let billDetail = queryDetailSerivce(kdSession(),req.query.billNumber,req.query.billType);


        try{
            let queryRet = JSON.parse(billDetail);
            if(queryRet.Result.ResponseStatus){
                console.log("++++++++++++++++++++++++++++++++++++++")
                res.json({'status': 500, 'data': "no data find"});
                return null;
            }
        }catch (e){

            console.log("----------------------" + e)
            res.json({'status': 500, 'data': "no data find"});
            return null;
        }



        let path = null;
        if(req.param("billType") == "STK_InStock"){
            path = DealInStock(billDetail, "STK_InStock");
        }

        var cdBqExport = new CdbqExport({
            expTime: cmoment().format('YYYY-MM-DD HH:mm:ss'),
            expType: req.query.billType,
            billNumber: req.query.billNumber,
            fileName : path.fileName,
            repPath: path.repPath,
            downLoadPath: path.downLoadPath,
        });

        yield cdBqExport.save();

        //res.download(cdBqExport.repPath);

        res.json({'status': 200, 'data': req.query.billNumber});

    }).catch(function (err) {
        console.log(err);
        return res.json({status: 500, message: err.message});
    });
});

// 查询导出详情
router.get('/downloadFile', pttAuth.isAuthenticated, function (req, res, next) {
    co(function*() {
        var exist = yield CdbqExport.findOne({billNumber: req.query.billNumber}).exec();

        res.download(exist.repPath);

    }).catch(function (err) {
        console.log(err);
        return res.json({status: 500, message: err.message});
    });
});

module.exports = router;